
<?php get_header(); ?>
		
		<section class="heading-contacts">
			<div class="section-wrapper">
				<h2><?php echo $post->post_title; ?></h2>
			</div>
		</section>
		
		<div class="container-iphone">		
		
			<?php 
			
				if(have_posts()) : while(have_posts()) : the_post();
					the_content();
					endwhile;
					endif;
			?>
		
			<ul class="sections-grid clearfix">	
		
				<li class="wrap-contact">
					<a href="<?php bloginfo('url'); ?>/kontakt/">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="<?php bloginfo('url'); ?>/buchung/">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		</div> <!-- /container-iphone -->
		<!--
		<section class="cta-block">
			
			<hgroup class="section-title-wrapper">
				<h2>Für weitere infos</h2>
				<p>Wenn du noch Fragen hast, beantworten wir sie gerne!</p>
				
				<div class="cta-button"><a href="<?php bloginfo('url'); ?>/kontakt/"><i class="icon-envelope-alt cta-icon"></i>Schreibe uns hier</a></div>
			</hgroup>

		</section>
		-->
		
<?php get_footer(); ?>