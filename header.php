<?php $my_post_type = get_post_type($post->ID);?>
<!doctype html>
<html lang="de">
<head>
	<!-- Meta -->
	<meta charset="utf-8">
	
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">

	
	<!-- Stylesheets -->
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/font-awesome.css">
	<!--[if lt IE 9]>
   		 <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

	
	<title><?php atracktive_theme_display_titles(); ?></title>
	<?php wp_head(); ?>
</head>
<body class="<?php if (is_home() || is_page('newsletter')) { echo('homepage'); } 
				    else if (is_page('angebot') || is_page('terminkalender') || is_archive()) { echo('tours-all'); } 
				    else if (is_page('uber-uns')) { echo('about-us'); }
				    else if (is_page('blog') || is_search() ) { echo('blog-all'); }
				    else if (is_page('kontakt')) { echo('contacts'); }
				    else if (is_page('faq')) { echo('faq'); }
				    else if (is_page('buchung')) { echo('reserve'); }
				    else if ($my_post_type == 'angebot') { echo('tours-single'); }
				    else if ($my_post_type == 'post') { echo('blog-single'); }
				    /*else if (is_archive()) { echo('tours-all'); }*/
				    else if (is_404()) { echo('error-404'); }
			  ?>">
	<div class="wrapper-iphone clearfix">
		<header class="main-header">
			<div class="section-wrapper clearfix">
			
			
			
			<a href="<?php bloginfo('url'); ?>">
				<div class="logo-mobile">
					<img src="<?php bloginfo('template_directory'); ?>/images/@2x/logo-atracktive-@2x.png" width="131" height="42" alt="Atracktive Mountainbiking">
				</div>
				
				<div class="logo-desktop">
					<img class="logo-desktop" src="<?php bloginfo('template_directory'); ?>/images/@2x/logo-atracktive-big-@2x.png"  width="220" height="70" alt="Atracktive Mountainbiking">
				</div>
			</a>
			

			<a href="#menu" class="menu-link"><i class="icon-reorder"></i></a>
			
			
			<?php atracktive_theme_custom_nav_menu(); ?>
			
			<div class="btn-reserve-wrapper">
				<a class="header-btn-reserve" href="http://www.atracktive.com/test/buchung/"><i class="icon-arrow-right cta-icon"></i>Buchung</a>
			</div> <!-- /btn-reserve-wrapper -->
			
			</div>
		</header>
