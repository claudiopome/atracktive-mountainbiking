<?php
/*
Template Name: Page - About us
*/
?>

	<?php get_header(); ?>
		
		<section class="heading-contacts">
			<div class="section-wrapper">
				<h2><?php echo $post->post_title; ?></h2>
			</div>
		</section>
		
		<div class="container-iphone">		
		
			<section class="about-text clearfix">
				
				
				
				
						
				<div class="inner">
				
				<section class="about-intro clearfix">
				<div class="section-wrapper-xs">
					<div class="footer-inner">
					
					<?php 
					
					if(have_posts()) : while(have_posts()) : the_post();
					the_content();
					endwhile;
					endif;
				?>
				
				
					<div class="text-block" id="axel-thumb">
						
						 <!-- <img src="images/axel-thumb.png" width="161" height="161" alt=""> -->
						 
						<p>&mdash; Axel Molinero, Inhaber von Atracktive</p>
					</div> <!-- /text-block -->
				</div>
				</div>
				</section>
					<section class="team-section">
					
						<div class="section-wrapper">
						
						<div class="footer-inner">
						
						<hgroup class="section-title-wrapper clearfix">
								<h2>Das Team</h2>
						</hgroup>
						
						
						<section class="team-wrapper">
							<ul class="team-list clearfix">
							
							<?php 
								
								$team = new WP_Query('post_type=team&orderby=name&order=ASC');
								$c = 0;
								while($team -> have_posts()) : $team -> the_post(); $c++;
								if($c == 2) {
									$style = 'col-last';
								}
								
								else $style = '';
								
								$post_image = atracktive_theme_fetch_post_image(); 
							?>
								<li <?php post_class($style); ?>>
									<a href="#" class="btn-inactive" data-section="<?php $title = $post->post_title; echo strtolower(str_replace(' ', '-', $title)) ?>"><?php the_title(); ?><i class="icon-plus-sign"></i></a>
									
									<div id="<?php echo strtolower(str_replace(' ', '-', $title)); ?>" class="clearfix closed">
										
										
										<header class="clearfix">
											<h4><?php the_title(); ?></h4>
										</header>
										
										<?php the_content(); ?>
										
									</div> <!-- /axel -->
								</li>
								
							<?php endwhile; ?>
								
								<!--
								
								<li class="col-last">
									
									<a href="#" class="btn-inactive" data-section="massimo-casorzo"><i class="icon-plus-sign"></i>Massimo Casorzo</a>
									
									<div id="massimo-casorzo" class="closed">
										<figure>
											<img src="<?php bloginfo('template_directory') ?>/images/team/massimo-big.jpg">
										</figure>
										
										
										<h4>Massimo Casorzo</h4>
										
										<p class="info-body">Die Berge, die Natur und der Sport sind seine großen Leidenschaften. Seit seiner Jugend liebt es Massimo, Sport im Freien zu treiben.</p>
										
										<p class="info-body">Seine Leidenschaft gilt vor allem dem Skilanglauf, den er wettkampfmäßig betreibt. Im Sommer tauscht Massimo die Ski gegen sein Bike ein.</p>
										
										<p class="info-body">Das Mountainbiken hält ihn während der langen Sommermonate fit und ermöglicht es ihm, seine Freiheitsdrang zu stillen. Seit Kindertagen fährt er auf den fantastischen Trails der Westalpentäler, wo er sich bestens auskennt. Bei jeder Fahrt gibt es etwas Neues, Überraschendes und Aufregendes zu entdecken.</p>
										
										<p class="info-body">Aus diesem Grund hat Massimo beschlossen, Biker zu guiden und mit seinen Entdeckungstouren zu begeistern. Die Zusammenarbeit mit Atracktive erlaubt es ihm, dies in professionellem Rahmen zu tun.</p>

									</div> <!-- /massimo --> 
								<!--</li> -->
							</ul>
						</section> <!-- /team-wrapper -->
						</div>
						</div>
					</section> <!-- /team-section -->

			</div>
				
		</section>

			
			<!--<section class="contact-info" id="info-mobile">
			
			<div class="contact-details">
				<div class="text-block">
					<i class="icon-home"></i>
					<span>Calsowstr. 13 - 37085 Göttingen</span>
				</div>
				
				<div class="text-block">
					<ul>
						<li>
							<i class="icon-phone"></i>
								<a href="tel:+49 172 539 09 36"><span>+49 172 539 09 36</span></a>
						</li>
						<li>
							<i class="icon-envelope-alt"></i>
							
							<a href="mailto:info@atracktive.com"><span>info@atracktive.com</span></a>
						</li>
					</ul>
				</div>
			
		</section> -->

			
			<ul class="sections-grid clearfix">	
		
				<li class="wrap-contact">
					<a href="<?php bloginfo('url'); ?>/kontakt/">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="<?php bloginfo('url'); ?>/buchung/" class="open-register">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		</div> <!-- /container-iphone -->
		
		<section class="cta-block">
			
			<hgroup class="section-title-wrapper">
				<h2>Für weitere infos</h2>
				<p>Wenn du noch Fragen hast, beantworten wir sie gerne!</p>
				
				<div class="cta-button"><a href="<?php bloginfo('url'); ?>/kontakt/"><i class="icon-envelope-alt cta-icon"></i>Schreibe uns hier</a></div>
			</hgroup>

		</section>

		<?php get_footer(); ?>