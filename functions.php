<?php 
	
	/* THEME CUSTOM POST TYPES
	   --------------------------------------------------------------------------------- */
	    
	    add_action('init', 'atracktive_theme_create_post_types');
	    
	    function atracktive_theme_create_post_types() {
	    
	      
		     register_post_type('angebot', array(
		     		
		     		'labels' => array(
		     			'name' => __('Angebot'),
		     			'singular_name' => __('Angebot'),
		     		),
		     	 
		     		'show_ui' => true,
		     		'_builtin' => false,
		     		'capability_type' => 'post',
		    		'hierarchical' => false,
		    		'public' => true,
		    		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields'),
		    		'taxonomies' => array('angebot_cat', 'post_tag'),
		    	    )
		     );
		     
		    
		    register_post_type('team', array(
		    		
		    		'labels' => array(
		    			'name' => __('Team'),
		    			'singular_name' => __('Team'),
		    			
		    		 ),	
		    		 
		    		 'show_ui' => true,
		    		 '_builtin' => false,
		    		 'capability_type' => 'post',
		    		 'hierarchical' => false,
		    		 'public' => true,
		    		 'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields'),
		    		 'taxonomies' => array('post_tag'),
		    		 )
		       );

		}
    
        
    /* CUSTOM TAXONOMIES
       --------------------------------------------------------------------------------- */
  
      add_action('init', 'atracktive_theme_create_custom_taxonomies', 0);
  
      function atracktive_theme_create_custom_taxonomies() {
	  		$labels = array(
	  			'name' => _x('Angebot Categories', 'taxonomy general name'),
	  			'singular_name' => _x('Angebot Category', 'taxonomy singular name'),
	  			'search_items' => __('Search Angebot'),
	  			'all_items' => __('All Angebot Categories'),
	  			'parent_item' => __('Parent Angebot Category'),
	  			'parent_item_colon' => __('Parent Angebot Category'),
	  			'edit_item' => __('Edit Angebot Category'),
	  			'update_item' => __('Update Angebot Category'),
	  			'add_new_item' => __('Add New Angebot Category'),
	  			'new_item_name' => __('New Angebot Category Name'),
	  	     );
	  
	  register_taxonomy('angebot_cat', array('angebot'),
	  		array(
	  			'hierarchical' => true,
	  			'labels' => $labels,
	  			'show_ui' => true,
	  			'query_var' => true,
	  			'rewrite' => array('slug' => 'angebot_categories')
	  		));
	   }

		 
	/* CUSTOM POST TYPES COLUMNS
	  --------------------------------------------------------------------------------- */
	  
	  add_filter('manage_edit-angebot_columns', 'atracktive_theme_angebot_columns');
	  add_action('manage_posts_custom_column', 'atracktive_theme_angebot_custom_columns');
	  
	  add_filter('manage_edit-team_columns', 'atracktive_theme_team_columns');
	  add_action('manange_posts_custom_column', 'atracktive_theme_team_custom_columns');
	  
	  function atracktive_theme_angebot_columns() {
		  $columns = array(
		  	'cb' => '<input type=\"checkbox\" />',
		  	"title" => __("Angebot"),
		  	"location" => __("Location"),
		  	"duration" => ("Duration"),
		  	"author" => __("Author"),
		  	"angebot_cats" => __("Categories"),
		  	"date" => __("Date")
		  );
	  
	  return $columns;

	  }
	  
	  function atracktive_theme_angebot_custom_columns($column) {
	 	 global $post;
	  
	 	 switch ($column) {
	 	 	
	 	 	
	 	 	case "location" : 
	 	 	$location = get_post_meta($post->ID, 'location', true);
	 	 	if (empty($location)) 
	 	 		echo __('Unknown');
	 	    else 
	 	    	echo $location;
	 	    
	 	    break;
	 	    
	 	 	case "duration" : 
	 	 	$duration = get_post_meta($post->ID, 'days', true);
	 	 	if (empty($duration)) 
	 	 		echo __('Unknown');
	 	    else 
	 	    	echo $duration;
	 	    
	 	    break;
		  
		 	 case "author" :
		 	 the_author();
		 	 break;
		 	 
		 	 case "angebot_cats" :
		 	 echo get_the_term_list($post->ID, 'angebot_cat', '', ', ', '');
		 	 break;
		 }
	  }
	  
	   function atracktive_theme_team_columns($columns) {
		   $columns = array(
		   	 "cb" => "<input type=\"checkbox\" />",
	  	     "title" => _x("Title", 'post title columns'),
	  	     "author" => _x("Author", 'post author column'),
	  	     "date" => _x("Date", 'post date column')
	      );
	  
	       return $columns;
       }
  
      function atracktive_theme_team_custom_columns($column) {
	      global $post;
	  
	      if ($column == "author") {
		     the_author();
	      }
       }
       
      
	  /* CUSTOM POST TEMPLATE SUPPORT FOR CPT
        --------------------------------------------------------------------------------- */
		
      function atracktive_theme_cpt_post_types( $post_types ) {
			$post_types[] = 'angebot';
			return $post_types;
		
	  }
	  
	  add_filter( 'cpt_post_types', 'atracktive_theme_cpt_post_types' );
		 

  
	 /* CUSTOM POST TYPES DASHBOARD ICONS
       --------------------------------------------------------------------------------- */
      
      add_action('admin_head', 'atracktive_theme_custom_dashboard_icons');
      
      function atracktive_theme_custom_dashboard_icons() { ?>
	      
	      <style type="text/css">
		      
		    #menu-posts-team .wp-menu-image {
			      background: url(<?php bloginfo('template_directory') ?>/images/dashboard/team-icons.png) no-repeat -28px -3px !important;
            }
        
            #menu-posts-team:hover .wp-menu-image, #menu-posts-team.wp-has-current-submenu .wp-menu-image {
            	background-position: -2px -3px !important;	 
            } 

	      </style>
	      
      <?php } 
      
   
	/* REGISTER AND LOAD SITE SCRIPTS 
      --------------------------------------------------------------------------------- */
     
      
      function atracktive_theme_register_scripts() {
	  		if(!is_admin()) {
		  		wp_deregister_script('jquery');
		  		wp_register_script('jquery', 'http://code.jquery.com/jquery-latest.js', false, '1.9.1', true);
		  		wp_register_script('isotope', 'http://isotope.metafizzy.co/jquery.isotope.min.js', false, '1.5.25',  true);
		  		wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr.js', false, '1.7', true);
		  		wp_register_script('sitescripts', get_template_directory_uri() . '/js/app-ck.js', false, '1.0', true);
		  		wp_register_script('fluid-carousel', get_template_directory_uri() . '/js/fluid-carousel.js', false, '1.0', true);
		  		wp_register_script('tipsy', get_template_directory_uri() . '/js/jquery.tipsy.js', false, '1.0', true);
		  		
		  		wp_enqueue_script('jquery');
		  		wp_enqueue_script('isotope');
		  		wp_enqueue_script('modernizr');
		  		wp_enqueue_script('sitescripts');
		  		wp_enqueue_script('fluid-carousel');
		  		wp_enqueue_script('tipsy');
	  		}    
      }
      
       
      add_action('init', 'atracktive_theme_register_scripts');
      
   /* Widgets
     --------------------------------------------------------------------------------- */
     
     if (function_exists('register_sidebar')) {
	     
	     register_sidebar(array(
		   'name' => 'Twitter Widget',
		   'description' => 'Widget to displat twitter feed',  
	     ));
     }
      
      
    /* CATEGORIES 
      --------------------------------------------------------------------------------- */
      
      	define('BLOG', 3);

	
	/* SUPPORT FOR WP3 FEATURES
	  --------------------------------------------------------------------------------- */
	  
	  add_theme_support('post-thumbnails');
	  add_theme_support('nav-menus');
	  
	  if(function_exists('wp_nav_menu')) {
		  register_nav_menu('primary_nav', 'Primary Navigation');
	  }
	  
	  function atracktive_theme_custom_nav_menu() {
		  if (function_exists('wp_nav_menu')) :
		  	
		  	wp_nav_menu(
		  		array(
		  			'container' => 'nav',
		  			'container_id' => 'menu',
		  			'depth' => 2
		  		)
		  	);
		  	
		  	else:
		  	
		  	 echo '<nav id="menu">';
		  	 echo '<ul>';
		  	 wp_list_pages('depth=1&title_li=');
		  	 echo '</ul>';
		  	
		  	endif;
	  }
	 
	   
	  /* DISPLAY PAGE TITLES
	    --------------------------------------------------------------------------------- */
	    
	    function atracktive_theme_display_titles() {
		    
		    if(!$separator) {
			    $separator = "|";
			}
			    
			if(is_front_page()) {
				 bloginfo('name'); echo $separator; bloginfo('description');
			}
			    
		    else if(is_single() or is_page() or is_home()) {
			     bloginfo('name');
				 wp_title($separator, true, '');
			}
			    
			else if(is_404()) {
				  bloginfo('name');
				  echo " $separator ";
				  echo('Seite nicht gefunden');
			}
			    
			else {
				 bloginfo('name');
				 wp_title($separator, true, '');
			}
	    }
	    
	    
	 /* POST IMAGE
	   --------------------------------------------------------------------------------- */
	   
	   function atracktive_theme_fetch_post_image() {
	   		
	   		global $post;
	   		$image = '';
	   		
	   		//Get the image from the post meta box
	   		$image = get_post_meta($post->ID, 'atracktive_post_image', true);
	   		if($image) return $image;
	   		
	   		//If the above doesn't exist, get the post thumbnail
	   		$image_id = get_post_thumbnail_id($post->ID);
	   		$image = wp_get_attachment_image_src($image_id, 'studioemme_thumb');
	   		$image = $image[0];
	   		if($image) return $image;
	
	
	   		//If there is still no image, get the first image from the post
	   		return studioemme_get_first_image();
	   }
	   
	 /* GETS THE FIRST IMAGE FROM THE POST
	   --------------------------------------------------------------------------------- */
	   
	   function studioemme_get_first_image() {
		   
		   global $post, $posts;
		   $first_img = '';
		   ob_start();
		   ob_end_clean();
		   $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
		
		   $first_img="";
		
		   if(isset($matches[1][0]))
		   		$first_img = $matches[1][0];
			
		   return $first_img;
	    }
	    
	 /* DISPLAY CUSTOM EXCERPT
	   --------------------------------------------------------------------------------- */
	   
	   function atracktive_theme_custom_excerpt($len=20, $trim="&hellip;") {
		   $limit = $len+1;
		   $excerpt = explode(' ', get_the_excerpt(), $limit);
		   $num_words = count($excerpt);
		   
		   if($num_words >= $len) {
			   $last_item = array_pop($excerpt);
		   }
		   
		   else {
			   $trim = '';
		   }
		   
		   $excerpt = implode(" ",$excerpt) . "$trim";
		   echo $excerpt;
	   }	
	   
	 /* INCLUDE ONLY BLOG POSTS IN SEARCH RESULTS
	   --------------------------------------------------------------------------------- */	   
	   
	   function atracktive_theme_search_filter($query) {
		   if ($query->is_search) {
			   $query->set('cat','3');
			}
			return $query;
		}

		add_filter('pre_get_posts','atracktive_theme_search_filter');
		
	 /* POST PAGINATION LINK ATTRIBUTES
	   --------------------------------------------------------------------------------- */	
	   
	   	add_filter('next_posts_link_attributes', 'posts_link_attributes');
	   	add_filter('previous_posts_link_attributes', 'posts_link_attributes');

	   	function posts_link_attributes() {
		   	return 'class="controls-toggle"';
		}
	  
	 /* THEME SHORTCODES
	   --------------------------------------------------------------------------------- */
	  
	   function atracktive_col_half_shortcode($atts, $content = null) {
		   return '<div class="col-half">' . do_shortcode($content) . '</div>';
	   }
	   add_shortcode('col-half', 'atracktive_col_half_shortcode');
	   
	   function atracktive_col_half_last_shortcode($attr, $content=null) {
		   return '<div class="col-half col-last">' . do_shortcode($content) . '</div>';
	   }
	   add_shortcode('col-half-last', 'atracktive_col_half_last_shortcode');
	   
	   function atracktive_orange_paragraph_shortcode($atts, $content = null) {
		   return '<p class="orange">' . do_shortcode($content) . '</p>';
	   }
	   add_shortcode('orange', 'atracktive_orange_paragraph_shortcode');
	   
	   function atracktive_body_paragraph_shortcode($atts, $content = null) {
		   return '<p class="info-body">' . do_shortcode($content) . '</p>';
	   }
	   add_shortcode('text-body', 'atracktive_body_paragraph_shortcode');
	   
	   function atracktive_small_heading($atts, $content = null) {
		   return '<h3 class="small-heading">' . do_shortcode($content) . '</h3>';
		}
	    add_shortcode('small-heading', 'atracktive_small_heading');
	    
	    function atracktive_post_image_shortcode($atts, $content = null) {
		   extract(shortcode_atts(array(
		   	'src' => 'http://www.atracktive.com/wp-content/uploads/2013/06/siersburg_0251.jpg',
		   ), $atts));
		   
		   $source = 'src="' . $src . '"';
		   
		   return '<figure class="post-photo"><img '. $source . '></figure>'; 
		   
		}
		add_shortcode('post-image', 'atracktive_post_image_shortcode');
	    
?>