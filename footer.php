<!-- FOOTER -->
		<footer class="footer-desktop clearfix">
			<div class="section-wrapper">
				<div class="footer-inner">
				<div class="col-one-third">
					<h6>Navigation</h6>
						
						<ul class="footer-left-col">
						<?php 
							wp_list_pages('include=10,56,127&title_li=');
						?>
							<!--<li><a href="tours-all.html">Angebot</a></li>
							<li><a href="uber-üns.html">Über üns</a></li>
							<li><a href="blog-all.html">Blog</a></li>
							<li><a href="kontakt.html">Kontakt</a></li> -->
						</ul> <!-- /.left-col -->
						
						<ul class="footer-right-col">
						<?php
							wp_list_pages('exclude=10,56,96,127,490&title_li=');
						?>
							<!--<li><a href="kontakt.html">Buchung</a></li>
							<li><a href="faq.html">FAQ</a></li> -->
						</ul> <!-- /.right-col -->
				</div> <!-- /.col-one-third -->
				
				<div class="col-one-third">
					<h6>Kontakt</h6>
					
					<ul class="footer-left-col">
						<li>Calsowstr. 13</li>
						<li style="margin-bottom:10px;">D-37085 Göttingen</li>
					</ul>
							
					<ul class="footer-right-col">
						<li>+49 172 539 09 36</li>
						<li><a href="mailto:info@atracktive.com">info@atracktive.com</a></li>
					</ul>

				</div> <!-- /.col-one-third -->
				
				<div class="col-one-third col-last newsletter">
					<h6>Newsletter</h6>
						<p>Wir halten Dich regelmäßig auf dem Laufenden.</p>
							<input type="email" name="email" id="email" placeholder="Email Adresse">
							<a href="http://eepurl.com/KdrVX" target="_blank" class="subscribe">Abbonieren</a>
						
				</div> <!-- /.col-one-third -->
			</div>
			</div>
		</footer> <!-- /FOOTER -->


		<footer class="footer-iphone clearfix">
			
			<div class="section-wrapper">
				
				<article class="copyright">
					<small>&copy; <?php echo date('Y');?> <?php bloginfo('name'); ?></small>
					<div class="impressum-links">
						<ul>
							<li><a href="#" id="impressum-link">Impressum</a></li>
							<li><a href="<?php bloginfo('url'); ?>/wp-content/uploads/2013/12/atracktive_agb.pdf" target="_blank">AGB</a></li>
						</ul>
					</div>
				</article>
			
				
				
				<article class="social-iphone">
				<ul class="clearfix">
					<li class="facebook-iphone">
						<a href="https://www.facebook.com/Atracktive?fref=ts" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/@2x/facebook-icon-@2x.png" width="30" height="30" alt="Facebook Icon"></a>
					</li>
					
					<li>
						<a href="https://twitter.com/Atracktive_mtb" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/@2x/twitter-icon-@2x.png" width="30" height="30" alt="Twitter Icon"></a>
					</li>
				</ul>
				
			</article>
			
		</footer>
		
		<section class="impressum clearfix">
			
			<div class="section-wrapper">
				
				<div class="impressum-details">
					
					
					<div class="col-one-third">
						<ul>
							<li>Axel Molinero</li>
							<li>(Kleinunternehmer i.S.d. § 19 UStG)</li>
							<li>Calsowstr. 13</li>
							<li>D-37085 Göttingen</li>
							<li>+49 172 539 09 36</li>
							<li><a href="mailto:info@atracktive.com">info@atracktive.com</a></li>
							<li><a href="<?php bloginfo('url'); ?>">www.atracktive.com</a></li>
						</ul>
					</div> <!-- col-one-third -->
					
					<div class="eightcol col-last">
						<ul>
							<li>Logo: Irene Felicetti</li>
							<li>Webseite: <a href="mailto:pomeee@me.com">Claudio Salatino</a></li>
							<li>Bilder: Axel Molinero, Lukas Stöckli, Stéphane Van Wonterghem, Florian Mair</li>
							<li>Partner: <a href="http://www.trailtech.de" target="_blank">Trailtech</a>, <a href="http://www.nmbiking.de" target="_blank">NM Biking</a>, <a href="http://www.plan-erlebnis.de" target="_blank">Plan E</a>, <a href="http://www.bikeaid.de" target="_blank">Bike AID</a>, <a href="http://www.sagnarotonda.com" target="_blank">Sagna Rotonda</a>, <a href="http://www.alpibikeresort.com" target="_blank">Alpi Bike Resort</a>, <a href="http://www.chaletdellaguida.it" target="_blank">Chalet della Guida</a>, <a href="http://www.turismotorino.org" target="_blank">Turismo Torino e Provincia</a></li>
						</ul>
						
						<p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen.</p>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- /wrapper-iphome  -->
	<div id="video-wrap">
		<div id="video"><!-- iframe will be placed by javascript --></div>
		<a href="#" id="close-video"><i class="icon-remove"></i></a>
	</div>
	
	<!--<section id="newsletter-modal" class="modal hide fade">
			<header class="modal-header clearfix">
				<div class="wrap-figure">
					<i class="icon-pencil"></i>
				</div>
				
				<h3>Newsletter</h3>
			</header>
		
			<section class="modal-body">
				
				<form class="clearfix" action="<?php bloginfo('template_directory'); ?>/php/process.php" method="post">
					<input type="email" name="email" id="email" placeholder="Email Adresse" class="required">
					<button type="submit" class="subscribe">Abschicken</button>
					<i class="icon-refresh icon-spin status-processing"></i>
					<i class="icon-ok-sign status-ok"></i>
					<i class="icon-remove-sign status-error"></i>
				</form>
			</section>
		</section> -->
		
	
	<?php wp_footer(); ?>
	
</body>
</html>
