<?php
/*
Template Name Posts: Single Tours
*/
?>

	<?php get_header(); ?>
	
	   <section class="heading-contacts clearfix">
			<div class="section-wrapper">
				<div class="tour-title">
					<h2><?php echo $post->post_title; ?></h2>
					<p><?php echo get_post_meta($post->ID, 'tour-subhead', true); ?> </p>
				</div>
			
				<nav class="tours-single-controls blog-single-controls">
					<a href="#" class="back-button" title="Back" ><i class="icon-th"></i></a><a href="#" class="prev-post" title="Previous Post"><i class="icon-chevron-left"></i></a><a href="#" class="next-post" title="Next Post"><i class="icon-chevron-right"></i></a>				
				</nav>
			</div>

		</section>
		
				
				
		<div class="container-iphone">	
			
			<div class="section-wrapper">		
				<section class="tours-info">
					<div class="contact-details">
					
					<div class="text-block">
						<ul class="info-icons">
							<li>
								<i class="icon-map-marker"></i>
								<span><?php echo get_post_meta($post->ID, 'location', true); ?></span>
							</li>
							
							<li>
								<i class="icon-time"></i>
								<span><?php echo get_post_meta($post->ID, 'days', true); ?></span>
							</li>
							
							<li>
								<i class="icon-tag"></i>
								<span><?php echo get_post_meta($post->ID, 'price', true); ?></span>
							</li>
						</ul>
						
						<!--<i class="icon-map-marker"></i>
						<span>Katalonien, Spanien</span>
					</div>
				
					<div class="text-block">
						<ul>
							<li>
								<i class="icon-time"></i>
								<span>5 tage</span>
							</li>
						
							<li>
								<i class="icon-tag"></i>
								<span>1355 €</span>
							</li>
						</ul>
					</div> -->
					</div>
				<hr>
			</div>
		</section> 
		<div class="section-wrapper">
			<section class="tours-text clearfix">
				<div class="inner">
				<?php 
					
					if(have_posts()) : while(have_posts()) : the_post();
					the_content();
					endwhile;
					endif;
				?>
				
				<?php get_sidebar('tours'); ?>
										
					<div class="clearfix"></div>
					
					<section class="program tours-section">
						<div class="col-half">
							
							<hr>
							<h2>Programm</h2>
							
							<dl>
								<dt>Samstag den 23. März</dt>
									<dd><p class="info-body">Treffpunkt ist um 22.00 Uhr am Genfer Bahnhof (CH). Von Genf aus reisen wir bequem im Nachtbus mit Bikeanhänger direkt zu unserem Hotel. Die Ankunft ist um ca. 7:30 Uhr. Nach einem Frühstück im Hotel besteht die Möglichkeit, sich noch ein wenig auszuruhen, bevor wir um 10:30 Uhr zu unserer ersten Tour aufbrechen.</p></dd>
								
								<dt>Sonntag - Samstag 24.-30. März</dt>
									<dd><p class="info-body">Auf dem Programm stehen 7 reizvolle Tagestouren in der katalanischen Vulkanregion „La Garrotxa“.</p></dd>
								
								<dt>Samstag den 30. März</dt>
									<dd><p class="info-body">Nach einer kurzen und anspruchsvollen „Abschlusstour“ kehren wir gegen 10:30 Uhr in unser Hotel zurück. Hier können wir duschen und uns auf die Rückfahrt vorbereiten. Die Rückfahrt ist um 11:00 Uhr. Ankunft am Genfer Bahnhof erfolgt gegen 20:00 Uhr.</p></dd>
								</dl>
							
							</div>
					</section>
					
					<section class="equipment tours-section">
						<div class="col-half col-last">
							
							<hr>
							<h2>Equipment</h2>
							
							<p class="info-body">Das ideale Rad ist ein nicht zu schweres, vollgefedertes All Mountain-Bike mit 120 - 140 mm Federweg. Die Teilnahme mit einem Hardtail bzw.einem Bike mit wenig Federung ist aber auch problemlos möglich. Zur Grundausstattung der Tour gehören Helm, Handschuhe sowie ein Rucksack mit Trinkblase und Multi-Tool.</p>
							
								<p class="info-body">Außerdem empfehlen wir, Radbrille, Ersatzschläuche, Bremsbeläge, Schaltauge und wetterfeste Funktionsbekleidung (z.B. langärmeliges T-Shirt, Windstopper/Regenjacke, Arm- und Beinlinge) mitzunehmen, da die Temperaturen morgens und abends niedrig sind und uns auch oberhalb von 1.000 m Höhe bewegen werden.</p>

						</div>
					</section>
					
					<!--<section class="more-info tours-section">
						<div class="col-half">
						<hr>
							<h2>Mehr Infos</h2>
								<a href="#" class="btn-inactive" data-section="included-not"><i class="icon-plus-sign"></i>Leistungen/Nicht incklusive</a>
								
								<div id="included-not" class="closed">
									
									<div id="included">
										<header class="clearfix">
											<div class="wrap-figure">
												<i class="icon-ok"></i>
											</div>
										<div class="wrap-details">
											<span>Leistungen</span>
										</div>
										</header>
										
										<div class="facts-wrap">
											<ul>
												<li>Included 1</li>
												<li>Included 2</li>
												<li>Included 3</li>
											</ul>
										</div>
									</div> <!-- /included -->
									
									<!--<div id="not-included">
										<header class="clearfix">
											<div class="wrap-figure">
												<i class="icon-remove"></i>
											</div>
											
											<div class="wrap-details">
												<span>Nicht inklusive</span>
											</div>
										</header>									
										<div class="facts-wrap">
											<ul>
												<li>Nicht Inklusive 1</li>
												<li>Nicht Inklusive 2</li>
												<li>Nicht Inklusive 3</li>
											</ul>
										</div>
									</div> <!-- /not-included -->
								<!--</div>
								
								<a href="#" class="btn-inactive" data-section="optionals"><i class="icon-plus-sign"></i>Optional</a>
								
								<div id="optionals" class="closed">
									<header class="clearfix">
										<div class="wrap-figure">
											<i class="icon-plus"></i>
										</div>
										
										<div class="wrap-details">
											<span>Optionals</span>
										</div>
									</header>									
									
									<div class="facts-wrap">
										<ul>
											<li>Optional 1</li>
											<li>Optional 2</li>
											<li>Optional 3</li>
										</ul>
									</div>
								</div>
						</div>
						</section> -->
											
						
						</div>
						
						
			</section>
		</div>
		
					
			<a href="#" class="more-like-this"><i class="icon-refresh icon-spin"></i>See more like this</a>

			<ul class="sections-grid clearfix">	
		
				
				<li class="wrap-contact">
					<a href="http://localhost:8888/atracktive/kontakt">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="http://localhost:8888/atracktive/kontakt" class="open-register">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		
		</div>
		
		</div>
		
		<section class="more-tours" id="desktop-overviews">
									
							<div class="section-wrapper">
							<hgroup class="section-title-wrapper">
								<h2>See more like this</h2>
							</hgroup>
								<div class="footer-inner">
									<ul class="grid-overviews clearfix">
										<li>
											<a href="#">
												<img src="images/events/thumbs/traversata-thumb.jpg" alt="Thumbnail">
												<div class="orange-bar"></div>
												<div class="tour-details">
													<hgroup>
														<h2>Traversata delle undici valli</h2>
														<p>Auf reise in die vergangenheit</p>
													</hgroup>
					    	
													<dl>
														<dt class="location"><i class="icon-map-marker"></i></dt>
														<dd class="location">Katalonien, Spanien</dd>
														<dt class="duration"><i class="icon-time"></i></dt>
														<dd class="duration">2 Tage</dd>
													</dl>
												</div> <!-- /tour-details -->
											</a>
										</li>
					
										<li>
											<a href="#">
												<img src="images/events/thumbs/traversata-thumb.jpg" alt="Thumbnail">
												<div class="orange-bar"></div>
												<div class="tour-details">
													<hgroup>
														<h2>Trailriderkamp Katalonien</h2>
														<p>Auf reise in die vergangenheit</p>
													</hgroup>
					    	
													<dl>
														<dt class="location"><i class="icon-map-marker"></i></dt>
														<dd class="location">Katalonien, Spanien</dd>
														<dt class="duration"><i class="icon-time"></i></dt>
														<dd class="duration">2 Tage</dd>
													</dl>
												</div> <!-- /tour-details -->
											</a>	
										</li>
										
										<li class="col-last">
											<a href="#">
												<img src="images/events/thumbs/traversata-thumb.jpg" alt="Thumbnail">
												<div class="orange-bar"></div>
												<div class="tour-details">
													<hgroup>
														<h2>Bike Camp Val Susa II</h2>
														<p>Auf reise in die vergangenheit</p>
													</hgroup>
					    	
													<dl>
														<dt class="location"><i class="icon-map-marker"></i></dt>
														<dd class="location">Katalonien, Spanien</dd>
														<dt class="duration"><i class="icon-time"></i></dt>
														<dd class="duration">2 Tage</dd>
													</dl>
												</div> <!-- /tour-details -->
											</a>	
										</li>		
									</ul>
								</div>
									</div>
								</section>
				
		<section class="cta-block">
			<hgroup class="section-title-wrapper">
				<h2>Bist du startbereit?</h2>
				<p>Um das Anmeldeformular auszufüllen, bitte hier klicken</p>
				
				<div class="cta-button"><a href="buchung.html">Buchung</a></div>
			</hgroup>
		</section>
		

		<?php get_footer(); ?>