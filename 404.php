<?php get_header(); ?>
		<div class="container-iphone">		
		
		<section class="page-404">
			<div class="section-wrapper">
				<h2>Hoppla...</h2>
				
				<h4>Fehler 404</h4>
				<p class="info-body">Etwas ist schiefgelaufen oder die von Ihnen gesuchte Seite existiert leider nicht.</p>
				<div class="cta-button"><a href="<?php bloginfo('url'); ?>">Zurück zur Startseite</a></div>
			</div>
		</section>


			<ul class="sections-grid clearfix">	
		
				<li class="wrap-contact">
					<a href="kontakt.html">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="buchung.html">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		</div>
		
<?php get_footer(); ?>