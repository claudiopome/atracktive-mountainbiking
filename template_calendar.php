<?php
/*
Template Name: Page - Calendar
*/

?>

<?php get_header(); ?>	
		<section class="heading-contacts clearfix">
			<div class="section-wrapper">
				<div class="tour-title">
					<h2><?php echo $post->post_title; ?></h2>
				</div>
			
				<nav class="tours-single-controls blog-single-controls">
									 	
				 	<a href="#" class="controls-toggle" data-section="categories" original-title="Kategorien"><i class="icon-list-ul"></i></a>
				 	
				</nav>
			</div>

		</section>
		
		<!--<section class="categories-container clearfix" id="categories">
			<div class="section-wrapper">
				<ul>
					<li><span class="select-cat">Auswählen:</span></li>
					<li><a href="<?php bloginfo('url');?>/angebot/" data-filter="*" class="selected">Alles</a></li>
					<?php /*$terms = get_terms("angebot_cat", "order=DESC&hide_empty=0");
								$count = count($terms);
								if ( $count > 0 ){
								
									foreach ( $terms as $term ) {
										echo '<li><a href="' . get_term_link($term->slug, 'angebot_cat').'">' . $term->name . '</a></li>';        
									}
							     } 
						   */?>
				</ul>
			</div>
		</section> -->
		
				
				
		<div class="container-iphone">	
			
		<div class="section-wrapper-s">
		
			<section class="more-tours">
			
			<section class="month-wrap">
				
				<h2>März</h2>
				<hr>
				
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">März</span>
						<span class="date-number">22</span>
						<span class="day">Samstag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Technikcamp Katalonien</h3>
						<p>Saisonauftakt auf hohem Niveau</p>
					</div>
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>Katalonien, Spanien</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>7 Tage</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="<?php bloginfo('url'); ?>/angebot/technikcamp-katalonien">Mehr</a>
				</div> <!-- /calendar-overview -->
			
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">März</span>
						<span class="date-number">22</span>
						<span class="day">Samstag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Trailridercamp Katalonien</h3>
						<p>Biken in mediterranem Flair</p>
					</div>
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>Katalonien, Spanien</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>7 Tage</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="<?php bloginfo('url'); ?>/angebot/trailridercamp-katalonien">Mehr</a>
				</div> <!-- /calendar-overview -->
			
			
			</section> <!-- /month-wrap -->
				
			<section class="month-wrap">
				
				<h2>April</h2>
				<hr>
				
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">April</span>
						<span class="date-number">05</span>
						<span class="day">Samstag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Drëi-Lander Cross</h3>
						<p>Jubiläumstour: 5 Jahre Atracktive</p>
					</div>
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>Deutschland, Luxemburg, Frankreich</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>2 Tage</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="#">Mehr</a>
				</div> <!-- /calendar-overview -->
				
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">April</span>
						<span class="date-number">26</span>
						<span class="day">Samstag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Technikamp Saarland</h3>
						<p>Fahrtechniktraining für Fortgeschrittene</p>
					</div>
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>Siersburg, Saarland</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>2 Tage</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="#">Mehr</a>
				</div> <!-- /calendar-overview -->
			
			</section> <!-- /month-wrap -->
			
			<section class="month-wrap">
				
				<h2>Mai</h2>
				<hr>
								
				
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">Mai</span>
						<span class="date-number">31</span>
						<span class="day">Samstag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Luxemburg Cross</h3>
						<p>"Durchquerung eines Landes an einem einzigen Tag</p>
					</div>
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>Luxemburg</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>1 Tag</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="#">Mehr</a>
				</div> <!-- /calendar-overview -->
			
			</section> <!-- /month-wrap -->
			
			<section class="month-wrap">
				
				<h2>Juni</h2>
				<hr>
				
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">Juni</span>
						<span class="date-number">01</span>
						<span class="day">Sonntag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Fahrtechnik Basic</h3>
						<p>Fahrtechniktraining für Frauen</p>
					</div>
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>Mehrere Orte</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>2 Tage</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="#">Mehr</a>
				</div> <!-- /calendar-overview -->
				
				
				
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">Juni</span>
						<span class="date-number">15</span>
						<span class="day">Sonntag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Traversata delle undici valli</h3>
						<p>Auf Reise in die Vergangenheit</p>
					</div>
					
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>Piemont, Italien</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>7 Tage</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="#">Mehr</a>
				</div> <!-- /calendar-overview -->
			
			</section> <!-- /month-wrap -->
			
			<section class="month-wrap">
				
				<h2>Juli</h2>
				<hr>
				
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">Juli</span>
						<span class="date-number">06</span>
						<span class="day">Samstag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Piemont Cross</h3>
						<p>Vom hohen Susatal bis zur Poebene</p>
					</div>
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>Piemont, Italien</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>6 Tage</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="#">Mehr</a>
				</div> <!-- /calendar-overview -->
				
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">Juli</span>
						<span class="date-number">18</span>
						<span class="day">Freitag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Gipfeltour Schwarzwald</h3>
						<p>Auf schmalen Trails zu den höchsten Gipfeln</p>
					</div>
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>"Südschwarzwald, Deutschland</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>3 Tage</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="#">Mehr</a>
				</div> <!-- /calendar-overview -->
			
			</section> <!-- /month-wrap -->
			
			<section class="month-wrap">
				
				<h2>August</h2>
				<hr>
				
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">August</span>
						<span class="date-number">03</span>
						<span class="day">Sonntag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Bike Camp Val Susa</h3>
						<p>All Mountain-Genuss in den Westalpen</p>
					</div>
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>Piemont, Italien</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>6 Tage</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="#">Mehr</a>
				</div> <!-- /calendar-overview -->
			
			</section> <!-- /month-wrap -->
			
			<section class="month-wrap">
				
				<h2>September</h2>
				<hr>
				
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">September</span>
						<span class="date-number">13</span>
						<span class="day">Samstag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Bike &amp; Dolce Vita Piemont</h3>
						<p>Das Genusserlebnis für Biker</p>
					</div>
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>Piemont, Italien</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>6 Tage</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="#">Mehr</a>
				</div> <!-- /calendar-overview -->
				
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">September</span>
						<span class="date-number">21</span>
						<span class="day">Sonntag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Bike Camp Val Maira</h3>
						<p>Die Magie der Wildnis</p>
					</div>
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>Piemont, Italien</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>6 Tage</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="#">Mehr</a>
				</div> <!-- /calendar-overview -->
			
			</section> <!-- /month-wrap -->
			
			<section class="month-wrap">
				
				<h2>Oktober</h2>
				<hr>
				
				<div class="calendar-overview clearfix">
					
					<div class="orange-left"></div>
					
					<div class="date">
						<span class="month">Oktober</span>
						<span class="date-number">11</span>
						<span class="day">Samstag</span>
					</div> <!-- date -->
					
					<div class="calendar-title calendar-inner">
						<h3>Technikcamp Saarland</h3>
						<p>Fahrtechniktraining für Fortgeschrittene</p>
					</div>
					
					<div class="calendar-details calendar-inner">
						<ul>
							<li class="icon-tour-meta icon-location">
								<span>Siersburg, Saarland</span>
							</li>
							
							<li class="icon-tour-meta icon-days">
								<span>2 Tage</span>
							</li>
						</ul>
					</div>
					<a class="more-infos-calendar" href="#">Mehr</a>
				</div> <!-- /calendar-overview -->
			
			</section> <!-- /month-wrap -->

			
		</div>
		
		<ul class="sections-grid clearfix">	
		
				<li class="wrap-contact">
					<a href="<?php bloginfo('url'); ?>/kontakt/">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="<?php bloginfo('url'); ?>/buchung/" class="open-register">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		
		</div>
		
		</div>
		
				
		<section class="cta-block">
			
			<hgroup class="section-title-wrapper">
				<h2>Für weitere infos</h2>
				<p>Wenn du noch Fragen hast, beantworten wir sie gerne!</p>
				
				<div class="cta-button"><a href="<?php bloginfo('url'); ?>/kontakt/"><i class="icon-envelope-alt cta-icon"></i>Schreibe uns hier</a></div>
			</hgroup>

		</section>
		

<?php get_footer(); ?>