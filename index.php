<?php get_header(); ?>
		
		<section class="hero-unit">
			<div class="section-wrapper">
				<h1>Mountain bike training &amp; guiding</h1>
			</div>
			
			<div class="play-wrap">
				<a href="#" class="play-btn">Zum Video<i class="icon-play-circle"></i></a>
			</div>
		</section> <!-- /hero-unit -->
		
		<div class="container-iphone">
			
			
		
			<ul class="sections-grid clearfix">	
				<li class="wrap-season">
				<a href="<?php bloginfo('url');?>/angebot/">
					<img src="<?php bloginfo('template_directory'); ?>/images/mtb-icon.png" width="67" height="46" alt="mtb-icon">
					
					<hgroup class="section-head">
						<h2>Saison 2013</h2>
						</a>
					</hgroup>
				</li>
		
				<li class="wrap-blog">
					<a href="<?php bloginfo('url');?>/blog/">
					<img src="<?php bloginfo('template_directory'); ?>/images/blog-icon.png" width="46" height="46" alt="blog-icon">
					<hgroup class="section-head">
						<h2>Unser Blog</h2>
					</hgroup>
					</a>
					</li>
				
		
				<li class="wrap-contact">
					<a href="<?php bloginfo('url'); ?>/kontakt/">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="<?php bloginfo('url'); ?>/buchung/" class="open-register">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		
		
		</div> <!-- /container-iphone -->
		
		<section class="more-tours" id="desktop-overviews">
									
							<div class="section-wrapper">
							<hgroup class="section-title-wrapper">
								<h2>Saison 2013</h2>
								<p class="subhead-paragraph">Spektakuläre Crosse, lehrreiche Fahrtechnikkurse, neue Camps in frisch entdeckten Bike-Revieren. Viele Events stehen 2013 zur Auswahl. Das vielfältige <a class="subhead-link" href="<?php bloginfo('url'); ?>/angebot/">Angebot</a> lässt keine Wünsche offen!</p>

							</hgroup>
								<div class="footer-inner">
									<ul class="grid-overviews clearfix">
										
										<?php
											$tours = new WP_Query('post_type=angebot&showposts=3&orderby=rand');
											/*$tours = new WP_Query('cat=4&order=ASC');*/
											$c = 0;
											while($tours -> have_posts()) : $tours -> the_post(); $c++;
											if($c == 3) {
												$style = 'col-last';
												$c = 0;
											}
					
											else $style = '';
											$post_image = atracktive_theme_fetch_post_image(); 
										?>
				
										<li <?php post_class($style); ?>>
											<a href="<?php the_permalink(); ?>">
												<img src="<?php echo $post_image; ?>" alt="Thumbnail">
						
													<div class="tour-details">
														<hgroup>
															<p><?php echo get_post_meta($post->ID, 'tour-subhead', true); ?></p>
															<h2><?php the_title(); ?></h2>
														</hgroup>
					    	
														<dl>
															<dt class="location"><i class="icon-map-marker"></i></dt>
															<dd class="location"><?php echo get_post_meta($post->ID, 'location', true); ?></dd>
															<dt class="duration"><i class="icon-time"></i></dt>
															<dd class="duration"><?php echo get_post_meta($post->ID, 'days', true); ?></dd>
														</dl>
													</div> <!-- /tour-details -->
												
												<a href="<?php the_permalink(); ?>">
													<div class="excerpt-info">
														<hgroup>
															<p class="subhead"><?php echo get_post_meta($post->ID, 'tour-subhead', true); ?></p>
															<h2><?php the_title(); ?></h2>
														</hgroup>
							
														<dl>
															<dt class="location"><i class="icon-map-marker"></i></dt>
															<dd class="location"><?php echo get_post_meta($post->ID, 'location', true); ?></dd>
															<dt class="duration"><i class="icon-time"></i></dt>
															<dd class="duration"><?php echo get_post_meta($post->ID, 'days', true); ?></dd>
														</dl>
							
														<p class="quick-info"><?php atracktive_theme_custom_excerpt(19); ?></p>
							
													</div> <!-- excerpt-info -->
												</a>
											</a>
											</li>
					
										<?php endwhile; ?>
									</ul>
								
								</div>
									</div>
								</section>
			
			<!-- <section class="latest-posts">
			
			<div class="section-wrapper">
			
				<hgroup class="section-title-wrapper">
					  <h2>Letzte News</h2>
					  <p class="subhead-paragraph">Wir informieren wir dich topaktuell über Events rund um das Mountainbiken. <a class="subhead-link" href="<?php bloginfo('url'); ?>/blog/">Besuche unseren blog</a></p>
				</hgroup>
				
				<div class="footer-inner">
				
				<ul class="grid-overviews clearfix">
					<li>
						<a href="#">
							
							<div class="tour-details">
					    		<hgroup>
									<h2>Trail Festival Gardasee</h2>
									
								</hgroup>
								
								<div class="excerpt-wrapper">
									
									<p class="post-excerpt">Der Winter ist der ideale Zeitraum, um Ideen zu entwickeln, neue Projekte zu planen und die neue Bike-Saison vorzubereiten. Die unzähligen Gedanken, die einem einfallen, sollen aber [...]</p>
								
									<a class="read-more" href="blog-single.html">Weiterlessen  &gt;</a>
								</div>
								
								<dl>
									<dt class="location"><i class="icon-calendar"></i></dt>
									<dd class="location">26 April 2013</dd>
									<dt class="duration"><i class="icon-comment"></i></dt>
									<dd class="duration">1 Kommentar</dd>
								</dl>
							</div> 
						</a>
					</li>
					
					<li>
						<a href="#">
							
							<div class="tour-details">
					    		<hgroup>
									<h2>Mountainbiking im Val di Susa</h2>
									
								</hgroup>
								
								<div class="excerpt-wrapper">
									
									<p class="post-excerpt">Der Winter ist der ideale Zeitraum, um Ideen zu entwickeln, neue Projekte zu planen und die neue Bike-Saison vorzubereiten. Die unzähligen Gedanken, die einem einfallen, sollen aber [...]</p>
								
									<a class="read-more" href="blog-single.html">Weiterlessen  &gt;</a>
								</div>
								
								<dl>
									<dt class="location"><i class="icon-calendar"></i></dt>
									<dd class="location">26 April 2013</dd>
									<dt class="duration"><i class="icon-comment"></i></dt>
									<dd class="duration">2 Kommentare</dd>
								</dl>
							</div> 
						</a>	
					</li>
					
					<li class="col-last">
						<a href="#">
							
							<div class="tour-details">
					    		<hgroup>
									<h2>Fahrtechnikkurs in Siersburg</h2>
									
								</hgroup>
					    	
								<div class="excerpt-wrapper">
									
									<p class="post-excerpt">Der Winter ist der ideale Zeitraum, um Ideen zu entwickeln, neue Projekte zu planen und die neue Bike-Saison vorzubereiten. Die unzähligen Gedanken, die einem einfallen, sollen aber [...]</p>
								
									<a class="read-more" href="blog-single.html">Weiterlessen  &gt;</a>
								</div>
								
								<dl>
									<dt class="location"><i class="icon-calendar"></i></dt>
									<dd class="location">26 April 2013</dd>
									<dt class="duration"><i class="icon-comment"></i></dt>
									<dd class="duration">1 Kommentar</dd>
								</dl>
							</div> 
						</a>	
					</li>		
						

				</ul>
				</div>
			</div>
			</section> -->

		
		<section class="cta-block">
			
			<hgroup class="section-title-wrapper">
				<h2>Für weitere infos</h2>
				<p>Wenn du noch Fragen hast, beantworten wir sie gerne!</p>
				
				<div class="cta-button"><a href="<?php bloginfo('url'); ?>/kontakt/"><i class="icon-envelope-alt cta-icon"></i>Schreibe uns hier</a></div>
			</hgroup>

		</section>
		
<?php get_footer(); ?>