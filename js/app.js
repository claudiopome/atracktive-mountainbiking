$(document).ready(function() {
	
	$('.header-alert i').on('click', function(e) {
		e.preventDefault();
		
		$('.header-alert').css('display', 'none');
		
		console.log('goodbye alert');
	});

/* MENU TOGGLE
   --------------------------------------------------------------------------------- */
  $('body').addClass('js');
  	
  		$('body').on('click', '.icon-reorder', function(e) {
	    	
	    	$(this).removeClass('icon-reorder').addClass('icon-remove-sign'), $('.menu-link').css('background', '#ffa200');
	    	
	    	$('#menu').addClass('active');
	    	$('.heading-contacts, .hero-unit').addClass('top-shadow');
	    	e.preventDefault();
	    	e.stopPropagation();
	    }); 
	    
	    $('body').on('click', '.icon-remove-sign', function(e) {
	    	
	    	$(this).removeClass('icon-remove-sign').addClass('icon-reorder'), $('.menu-link').css('background', '#d0c4ae');
	    	
	    	$('#menu').removeClass('active');
	    	$('.heading-contacts, .hero-unit').removeClass('top-shadow');
	    	e.preventDefault();
	    	e.stopPropagation();
	    }); 
	    
	    $('.has-subnav > a').click(function(e) {
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass('active').next('ul').toggleClass('active');
		});
	    
 /* IMPRESSUM
   --------------------------------------------------------------------------------- */
   
   /*$('#impressum-link').on('click', function(e) {
   	    
   	   $('.impressum').slideToggle();
   	   $('body, html').animate({
	   	   scrollTop: $('.impressum').offset().top
   	   });
	   e.preventDefault();
   }); */
	    
   /* DROPDOWN SELECT
   --------------------------------------------------------------------------------- 
	    
	function DropDown(el) {
			this.dd = el;
			this.initEvents();
			}
				
			DropDown.prototype = {
				initEvents : function() {
					var obj = this;

					obj.dd.on('click', function(event){
					$(this).toggleClass('active');
					event.stopPropagation();
				});	
			}
	 }

	$(function() {

		var dd = new DropDown( $('#dd') );

		$(document).click(function() {
			// all dropdowns
			$('.wrapper-dropdown').removeClass('active');
		});
		
		$('.wrapper-dropdown .dropdown').find('a').click(function() {
			$('.wrapper-dropdown').removeClass('active');
		});

	}); */
	
	
	/* HOMEPAGE VIDEO LIGHTBOX
	 --------------------------------------------------------------------------------- */
	 
	 $('.play-btn').on('click', function(e) {
		e.preventDefault();
		
		var video_url = "https://www.youtube.com/embed/4gv0Gno4P_0?rel=0?autoplay=1";
		
		$("#video-wrap").fadeIn(700, function() {
				var e = $(window).height(),
					t = $(window).width() * .7,
					n = t * .5625,
					r = (e - n) * .45 + "px";
					r < 0 && (r = 0),
					$("#video").css("margin-top", r),
					$("#video").html('<iframe src="'+video_url+'" width="' + t + '" height="' + n + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>');

				});
		});
		
		$('#video-wrap').on('click', function() {
			$(this).fadeOut(500, function() {
				$('#video').html('');
			});
		});

	 
	/* ISOTOPE FILTERING
	 --------------------------------------------------------------------------------- */	
   
   var size = window.getComputedStyle(document.body,':after').getPropertyValue('content');
   
   if (size == 'large') {
	
	var $container = $('.tours-all .grid-overviews');
	$container.isotope({
		layoutMode: 'fitRows',
		animationOptions: {
			duration: 750,
			easing: 'linear',
			queue: false,
		}
	});
	
	/*
	$('.categories-container li').find('a').on('click', function(){
		var selector = $(this).attr('data-filter');
		console.log(selector);
		$container.isotope({
			filter: selector,
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false,	
			}
		});
		return false;
	});  
	
   } */
   
   lightboxInit();
   
   function lightboxInit() {
		
		$('.lb-trigger').click(function(e){
			if(size == 'large') {
				e.preventDefault();
				var imgpath = $(this).attr('href');
				buildLightBox(imgpath);
			}
		});
	}
		
	function buildLightBox(src) {
		$('<div class="lightbox">').appendTo('body').html('<img src="'+src+'" alt="" />');
			
		$('body').on('click', '.lightbox', function(e) {
			$('.lightbox').remove();
		});
	}
	/*
	$('.controls-toggle').tipsy(); 
	$('.controls-toggle-back').tipsy();
	*/
	/*$('.grid-overviews li').on('mouseenter', function(e) {
				$(this).addClass('hovered');
				
			})
			.on('mouseleave', function(e) {
				$(this).removeClass('hovered');	
			});*/ 

   
   }
   
   $(window).resize(function() {
		size = window.getComputedStyle(document.body,':after').getPropertyValue('content');
   });
   
   /* TOURS / BLOG CONTROLS 
     --------------------------------------------------------------------------------- */
     
     
     $('.controls-tours').on('click', function(e) {
	     //tooltip toggle
     	$(this).attr('original-title', 'Kategorien anzeigen')
     	if($(this).attr('original-title', 'Kategorien anzeigen')) {
	     	$(this).attr('original-title', 'Schlie�en Kategorien');
     	} 
     	
     	
     	//icon toggle    	
     	if($(this).children('i').hasClass('icon-remove')) {
	     	$(this).children('i').removeClass('icon-remove').addClass('icon-list-ul');
     	}
     	
     	else {
	     	$(this).children('i').removeClass('icon-list-ul').addClass('icon-remove');
     	}
     });
     
     $('.controls-toggle').on('click', function(e) {
     
     	//open section
     	var sectionToggle = '#' + $(this).attr('data-section');
     	$(sectionToggle).slideToggle('fast');
     	
     	e.preventDefault();
     });
     
     /*(function() {
	     var wrap = $('.categories-container');
	     var path = 'http://atracktive.com/test/wp-content/themes/atracktive/';
	     
	     
	     
	     $('.controls-toggle').on('click', function(e) {
	     
		    wrap.hide();
	     	
	     	var section = $(this).attr('data-section'),
	     	    target =  path + section + '.html';
	     	
	     	if (section == 'searchform') {
		    	wrap.attr('id', 'search-wrapper'); 	
	     	} 
	     	
	     	else {
		     	wrap.attr('id', 'categories');
	     	}
	     	
	     	wrap.load( target, function(response, status, xhr) {
		     	if (status == 'error') {
			     	var msg = 'Sorry, but there was an error:';
			     	alert(msg + xhr.status + ' ' + xhr.statusText);
		     	}
	     	});
	     	
	     	wrap.show();
	     	console.log(target);
		    e.preventDefault(); 
	     });
     })(); */
		
		
   /* REGISTER FORM SLIDE
     --------------------------------------------------------------------------------- */ 
	    
	/* $('body').on('click', '.open-register', function(e) {
	 	
	 	$(this).removeClass('open-register').addClass('close-register');
	 	
		$('body').find('#form-register')
			.removeClass('form-closed')
			.addClass('form-open'), 
		
		$('body, html').animate({
			scrollTop: $('#form-register').offset().top
		}, 500);
			 	
	 	e.preventDefault(); 
	 	e.stopPropagation();
	 }); 
	 
	 $('body').on('click', '.close-register', function(e) {
		
		 $('#form-register').removeClass('form-open').addClass('form-closed'), $(this).removeClass('close-register').addClass('open-register');
		 
		 $('body, html').animate({
			scrollTop: $(document).offset().top
		}, 500);
		
		e.preventDefault(); 
	 	e.stopPropagation();
	 }); */
	
   /* CONTACT FORM VALIDATION AND SUBMISSION
    ---------------------------------------------------------------------------------   
	 $('#abschicken').on('click', function(e) {
	 
	  $('.status').css('display', 'none');
	  $('.status ul').html('');
	 	
	 	var name = $('#name').val(),
	 		surname = $('#surname').val(),
	 		email = $('#email').val(),
	 		phone_number = $('#phone-number').val(),
	 		subject = $('#subject').val(),
	 		message = $('#message').val(),	 		
	 		error = [],
	 		message = ' is required ';
	 	
	 	if(name.length<1) {
			$('#name').css('border', '1px solid #ff581d');
			error.push('Name'+message);
	 	}
	 	
	 	if(surname.length<1) {
			$('#surname').css('border', '1px solid #ff581d');
			error.push('Vorname'+message);
	 	}
	 	
	 	
	 	if(email.length<1) {
		 	$('#email').css('border', '1px solid #ff581d');
			error.push('E-mail'+message);
	 	} else if (!email.match(/^([a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,4}$)/i)) {
		 	$('#email').css('border', '1px solid #ff581d');
			error.push('E-mail must be valid ');
	 	}
	 	
	 	if(phone_number.length<1) {
		 	$('#phone-number').css('border', '1px solid #ff581d');
			error.push('Telefon'+message);

	 	}
	 	
	 	if(subject.length<1) {
		 	$('#subject').css('border', '1px solid #ff581d');
			error.push('Betreff'+message);
		}
	 	
	 	if(message.length<1) {
		 	$('#message').css('border', '1px solid #ff581d');
			error.push('Nachricht'+message);
	 	}
	 	
	 	
	 	if(error != '') {
	 		
	 		console.log(error);
	 		$.each(error, function() {
		 		$('.status ul').append('<li>' + this + '</li>');
	 		});
	 		
	 		$('.status').css('display', 'block');
	 	} else {
		 	$.post('../php/mail.php', $('.form-contacts').serialize(), function(data) {});
		 	
		 	$('.status').text('Message sent!');
		 	$('.status').css('display', 'block');
		 	
	 	}
	 	
	 	
	 		
		 e.preventDefault();
	 });
	 
	 $('form input').each(function(i) {
		 $(this).change(function() {
			 if($(this).val().length>=1) {
				$(this).css('border', '1px solid #c8c9bb');		 
			 }
		 });
	 }); */
	 
	 $('<div></div>', {
		 'class' : 'clearfix',
	 }). insertAfter('.link');
	 
	 /* FORM RESET
	   --------------------------------------------------------------------------------- */
	 
	 $('#abbrechen').on('click', function(e) {
	 	
	 	$('form input[type="text"] , form input[type="email"], form input[type="tel"], form textarea').each(function() {
	 		if($(this).val() != '')
		 		$(this).val('');
	 	});
		 e.preventDefault();
	 });
	 
		   
	  /* MORE INFOS ACCORDION
	   --------------------------------------------------------------------------------- */  
	   
	   $('body').on('click', '.btn-inactive', function(e) {
		  
		  var sectionToOpen = '#' + $(this).attr('data-section');
		  	  
		  	  console.log(sectionToOpen);
		  	  $(sectionToOpen).removeClass('closed').addClass('open'), $(this).children('i.icon-plus-sign').removeClass('icon-plus-sign').addClass('icon-minus-sign');
		  	  
		  	  var ws = window.getComputedStyle(document.body,':after').getPropertyValue('content');
		  	  
		  	  if (!ws) {
		  	  
		  	  	$('body, html').animate({
			  		  scrollTop: $(this).offset().top - 10
			  	}, 500);
		  	  
		  	  }
		  	  
		  	  $(this).removeClass('btn-inactive').addClass('btn-active');
		  	  
		  e.preventDefault();
		  e.stopPropagation();
	   });
	   
	   $('body').on('click', '.btn-active', function(e) {
	   		
	   	  var sectionToClose = '#' + $(this).attr('data-section');
	   	     
	   	      $(sectionToClose).removeClass('open').addClass('closed');
	   	      
	   	       $(this).children('i.icon-minus-sign').removeClass('icon-minus-sign').addClass('icon-plus-sign');
	   	       
	   	        $(this).removeClass('btn-active').addClass('btn-inactive');
	   	   	
		   e.preventDefault();
		   e.stopPropagation();
	   });
	   
	   
	   $('.more-like-this').on('click', function(e) {
	   	   
	   	   $('.more-like-this .icon-spin').css('display', 'inline-block');
	   	   
	   	   setTimeout(function() {
		   	   $.ajax({
		   	   type: 'GET',
		   	   url: 'see-more.html',
		   	   dataType: 'html',
		   	   success: function(html, textStatus) {
			   	   $('.more-like-this').after(html),
			   	   $('.more-like-this .icon-spin').css('display', 'none');
		   	   },
		   	   error: function(xhr, textStatus, errorThrown) {
			   	   alert('An error occurred! ' + (errorThrown ? errorThrown : xhr.status));
			   	   $('.more-like-this .icon-spin').css('display', 'none');
		   	   }
	   	   });

	   	   }, 3000);
	   	   	   	   
		   e.preventDefault();
	   });
	 
	 /* HTML5 PLACEHOLDER FIX 
	   --------------------------------------------------------------------------------- */
	   
	   if(!Modernizr.input.placeholder) {
		   
		   $('[placeholder]').focus(function() {
			   var input = $(this);
			   if(input.val() == input('placeholder')) {
				   input.val('');
				   input.removeClass('placeholder');
			   }
		   }).blur(function() {
			   var input = $(this);
			   if(input.val=='' || input.val() == input.attr('placeholder')) {
				   input.addClass('placeholder');
				   input.val(input.attr('placeholder'));
			   }
		   }).blur();
		   $('[placeholder]').parents('form').submit(function() {
			  $(this).find('[placeholder]').each(function(){
				  var input = $(this);
				  if(input.val() == input.attr('placeholder')) {
					  input.val('');
				  }
			  }) 
		   });
	   }
}); //end document.ready();


