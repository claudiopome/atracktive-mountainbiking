 /* FLUID CAROUSEL
	   --------------------------------------------------------------------------------- */
	   
	  (function(w) {
		  
		   		var sw = document.body.clientWidth,
				current = 0,
				breakpointSize = window.getComputedStyle(document.body,':after').getPropertyValue('content'),
				multiplier = 1,
				
				$carousel = $('.carousel-wrap'),
				$cList = $('.carousel-wrap ul'),
				$cContainer = $('.carousel-list-wrap'),
				$cWidth = $cContainer.outerWidth(),
				cLeft = $cList.css("left").replace('px',''),
				$li = $cList.find('li'),
				$liLength = $li.size(),
				numPages = $liLength/multiplier,
				$prev = $('.prev'),
				$next = $('.next');
				
				
				
				$(document).ready(function() {
					buildCarousel();
					
				});
				
				$(window).resize(function() {
					sw = document.body.clientWidth;
					$cWidth = $cContainer.width();
					breakpointSize = window.getComputedStyle(document.body,':after').getPropertyValue('content');
					
					sizeCarousel();
					posCarousel();
				});
				
				function sizeCarousel() {
					current = 0;
					
					animLimit = $liLength/multiplier-1;
					$li.outerWidth($cWidth/multiplier);
				}
				
				function buildCarousel() {
					sizeCarousel();
					
					if(Modernizr.touch) {
						buildSwipe();
					}
				}
				
				function posCarousel() {
					var pos = -current * $cWidth;
					$cList.addClass('animating').css('left',pos);
					
					setTimeout(function() {
						$cList.removeClass('animating');
						$cLeft = $cList.css('left').replace('px','');
					}, 500);
				}
				
				$prev.on('click', function(e) {
					e.preventDefault();
					moveRight();
				});
				
				$next.on('click', function(e) {
					e.preventDefault();
					moveLeft();
				});
				
				function moveRight() {
					if(current>0) {
						current--;
					}
					
					posCarousel();
				}
				
				function moveLeft() {
					if(current<animLimit) {
						current++;
					}
					
					posCarousel();
				}
  
				function buildSwipe() {
					var threshold = 80,
					origX = 0,
					finalX = 0,
					changeX = 0,
					changeY = 0,
					curPos;
		    
				
				cContainer.get(0).addEventListener("touchstart", function (event) {
					origX = event.targetTouches[0].pageX;
					curPos = origX;
				});
		
				
				cContainer.get(0).addEventListener("touchmove", function (event) {
					finalX = event.touches[0].pageX,
					diffX = origX - finalX,
					leftPos = cLeft-diffX;
        
					event.preventDefault();
					$cList.css("left",leftPos);
				});
		
				
				cContainer.get(0).addEventListener("touchend", function (event) {
					var diffX = origX - finalX,
					diffXAbs = Math.abs(diffX);
      
					if (diffX > 0 && diffXAbs > threshold) {
						moveLeft();
					} else if (diffX < 0 && diffXAbs > threshold) {
						moveRight();
					} else {
						posCarousel();
					}
			
					origX = finalX = diffX = 0;
				});
			}
	   })(this);
	   
	  /* CAROUSEL NAV ROLLOVERS
	  --------------------------------------------------------------------------------- */
	  
	  var ws = window.getComputedStyle(document.body,':after').getPropertyValue('content');
	  	
	  	if(ws) {
		 
		  $('.carousel-wrap').on('mouseenter mouseleave', function(e) {
		  		if(e.type == 'mouseenter') {
			 
			  		$(this).find('nav').children('a').css('opacity', '0.7');
			  	}
		  
			  	else {
				  $(this).find('nav').children('a').css('opacity', '0');
				}
			});
			
		}
