<?php get_header(); ?>
		<section class="heading-contacts clearfix">
			<div class="section-wrapper">
				<div class="tour-title">
					<h2><?php echo $post->post_title; ?></h2>
				</div>
			
				<nav class="tours-single-controls blog-single-controls">
					
					<a href="#" class="controls-toggle controls-tours" data-section="categories" title="Kategorien"><i class="icon-remove"></i></a><a href="#" class="controls-toggle" data-section="search-wrapper" title="Suchen"><i class="icon-search"></i></a>
					
					
		
				</nav>
				
			</div>
				

		</section>
		
		<section class="categories-container clearfix" id="categories">
						<div class="section-wrapper">
							<ul>
								<li><span>Auswählen:</span></li>
								<li><a href="http://www.atracktive.com/" data-filter="*" class="selected">Archiv</a></li>
								<?php wp_list_categories('title_li=&order=ASC&child_of=3'); ?>
								
							</ul>
						</div> 
					</section>
					
					<section class="categories-container clearfix" id="search-wrapper">
						<div class="section-wrapper">
							<?php include TEMPLATEPATH . '/searchform.php'; ?>
						</div>
					</section> 
			</div>
				
				
		<div class="container-iphone">	
			
		<div class="section-wrapper">
		
			<section class="latest-posts">
				
				<ul class="grid-overviews clearfix">
					<?php
						
						$tours = new WP_Query('cat=3');
						$c = 0;
						while($tours -> have_posts()) : $tours -> the_post(); $c++;
							
							if($c == 3) {
								$style = 'col-last';
								$c = 0;
							}
					
						else $style = '';
					?> 

					<li <?php post_class($style); ?>>
						<a href="<?php the_permalink(); ?>">
							
							<div class="tour-details">
					    		<hgroup>
									<h2><?php the_title(); ?></h2>
									
								</hgroup>
								
								<div class="excerpt-wrapper">
									
									<p class="post-excerpt"><?php atracktive_theme_custom_excerpt(28); ?></p>
								
									<a class="read-more" href="<?php the_permalink(); ?>">Weiterlesen  &gt;</a>
								</div>
								
								<dl>
									<dt class="location"><i class="icon-calendar"></i></dt>
									<dd class="location"><?php the_date('j F, Y'); ?></dd>
							   <!-- <dt class="duration"><i class="icon-comment"></i></dt>
									<dd class="duration">1 Kommentar</dd> -->
								</dl>
							</div> <!-- /tour-details -->
						</a>
					</li>
					
					<?php endwhile; ?>
				</ul>
			
			</section>

		</div>
		
		<ul class="sections-grid clearfix">	
		
				<li class="wrap-contact">
					<a href="<?php bloginfo('url'); ?>/kontakt/">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="<?php bloginfo('url'); ?>/buchung/" class="open-register">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		
		</div>
		
		</div>
		
		<!--		
		<section class="cta-block twitter">
			
			<hgroup class="section-title-wrapper">
				
				<div class="twitter-icon">
					<i class="icon-facebook"></i>	
				</div>
		
				
				<p class="follow">Folge Atracktive auf <a href="https://www.facebook.com/Atracktive?fref=ts" target="_blank">Facebook</a></p>
			</hgroup>
		</section>
		-->

<?php get_footer(); ?>