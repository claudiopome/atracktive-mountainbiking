<?php
/*
Template Name: Page - Blog
*/
?>


<?php get_header(); ?>
		<section class="heading-contacts clearfix">
			<div class="section-wrapper">
				<div class="tour-title">
					<h2><?php echo $post->post_title; ?></h2>
				</div>
			
				<nav class="tours-single-controls blog-single-controls">
					
					<a href="#" class="controls-toggle" data-section="categories" title="Kategorien"><i class="icon-list-ul"></i></a><a href="#" class="controls-toggle" data-section="search-wrapper" title="Suchen"><i class="icon-search"></i></a>
					
					
		
				</nav>
				
			</div>
				

		</section>
		
		<section class="categories-container clearfix" id="categories">
						<div class="section-wrapper">
							<ul>
								<li><span>Auswählen:</span></li>
								<li><a href="#" data-filter="*" class="selected">Alles</a></li>
								<?php wp_list_categories('title_li=&order=ASC&child_of=6'); ?>
								
							</ul>
						</div>
					</section>
					
					<section class="categories-container clearfix" id="search-wrapper">
						<div class="section-wrapper">
							
							<form method="get" id="searchform" action="" />
								<input type="text"  name="s" id="s" placeholder="Suchen..." />
								<input class="small-button" id="search-button" type="submit"  value="Suchen" />
							</form>		
						</div>
					</section>
			</div>
				
				
		<div class="container-iphone">	
			
		<div class="section-wrapper">
		
			<h4>Search results for: '<?php echo($s); ?>' </h4>
		
			<section class="latest-posts">
				
				<ul class="grid-overviews clearfix">
					<?php
						
						
						$c = 0;
						while(have_posts()) :  the_post(); $c++;
							
							if($c == 3) {
								$style = 'col-last';
								$c = 0;
							}
					
						else $style = '';
					?> 

					<li <?php post_class($style); ?>>
						<a href="<?php the_permalink(); ?>">
							
							<div class="tour-details">
					    		<hgroup>
									<h2><?php the_title(); ?></h2>
									
								</hgroup>
								
								<div class="excerpt-wrapper">
									
									<p class="post-excerpt"><?php atracktive_theme_custom_excerpt(28); ?></p>
								
									<a class="read-more" href="<?php the_permalink(); ?>">Weiterlessen  &gt;</a>
								</div>
								
								<dl>
									<dt class="location"><i class="icon-calendar"></i></dt>
									<dd class="location"><?php the_date('j F, Y'); ?></dd>
							   <!-- <dt class="duration"><i class="icon-comment"></i></dt>
									<dd class="duration">1 Kommentar</dd> -->
								</dl>
							</div> <!-- /tour-details -->
						</a>
					</li>
					
					<?php endwhile; ?>
				</ul>
			
			</section>

		</div>
		
		<ul class="sections-grid clearfix">	
		
				<li class="wrap-contact">
					<a href="<?php bloginfo('url'); ?>/kontakt/">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="<?php bloginfo('url'); ?>/buchung/" class="open-register">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		
		</div>
		
		</div>
		
				
		<section class="cta-block">
			
			<hgroup class="section-title-wrapper">
				
				<div class="twitter-icon">
					<i class="icon-twitter"></i>	
				</div>
				
				<p>Ah'll learn thi mardy bum <a href="#">@Nobbut</a> a lad ne'ermind will 'e 'eckerslike th'art nesh thee.</p>
				
				<p class="follow">Folge Atracktive auf <a href="#">Twitter</a></p>
			</hgroup>
		</section>
		

<?php get_footer(); ?>