<?php
/*
Template Name: Page - Contacts
*/
?>


<?php get_header(); ?>		
		<section class="heading-contacts">
			<div class="section-wrapper">
				<h2>Nimm mit uns kontakt auf</h2>
			</div>
		</section>
		
		<section class="contact-info" id="info-desktop">
			<div class="contact-details">
			<div class="section-wrapper">
				<div class="text-block">
					<i class="icon-home"></i>
					<span>Calsowstr. 13 - D-37085 Göttingen</span>
				</div>
				
				<div class="text-block">
					<ul>
						<li><i class="icon-phone"></i><a href="tel:+49 172 539 09 36"><span>+49 172 539 09 36</span></a></li>
						<li><i class="icon-envelope-alt"></i><a href="mailto:info@atracktive.com"><span>info@atracktive.com</span></a></li>
					</ul>
				</div>
			</div>
			</div>
		</section> 
		
		<div class="container-iphone">		
		
		
			
			<section class="contact-info" id="info-mobile">
			
			<div class="contact-details">
				<div class="text-block">
					<i class="icon-home"></i>
					<span>Calsowstr. 13 - 37085 Göttingen</span>
				</div>
				
				<div class="text-block">
					<ul>
						<li>
							<i class="icon-phone"></i>
								<a href="tel:+49 172 539 09 36"><span>+49 172 539 09 36</span></a>
						</li>
						<li>
							<i class="icon-envelope-alt"></i>
							
							<a href="mailto:info@atracktive.com"><span>info@atracktive.com</span></a>
						</li>
					</ul>
				</div>
			
		</section> 

			<section class="form form-contacts">
			
			<div class="section-wrapper">
			
			<?php 
				if (have_posts()) : while(have_posts()) : the_post();
				the_content();
				endwhile;
				endif;
			?>
					
			</div>
			</section>
		
			<ul class="sections-grid clearfix">	
		
				<li class="wrap-contact">
					<a href="<?php bloginfo('url'); ?>/kontakt/">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="<?php bloginfo('url'); ?>/buchung/" class="open-register">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		</div>
		
		<section class="cta-block twitter">
			
			<hgroup class="section-title-wrapper">
				
				<div class="twitter-icon">
					<i class="icon-twitter"></i>	
				</div>
				
				<div class="section-wrapper">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Twitter Widget')); ?>
				</div>
				
				<p class="follow">Folge Atracktive auf <a href="https://twitter.com/Atracktive_mtb" target="_blank">Twitter</a></p>
			</hgroup>
		</section>

<?php get_footer(); ?>