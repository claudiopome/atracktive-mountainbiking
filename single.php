<?php $my_post_type = get_post_type($post->ID);?>
<?php get_header(); ?>		

<?php

if ($my_post_type == 'angebot') : ?>
	
	<section class="heading-contacts clearfix">
			<div class="section-wrapper">
				<div class="tour-title">
					<h2><?php echo $post->post_title; ?></h2>
					<p><?php echo get_post_meta($post->ID, 'tour-subhead', true); ?> </p>
				</div>
			
				<nav class="tours-single-controls blog-single-controls">
					<a href="<?php bloginfo('url'); ?>/angebot" class="back-button controls-toggle-back" original-title="Angebot"><i class="icon-th"></i></a><?php next_post_link('%link', '<i class="icon-chevron-left"></i>'); ?><?php previous_post_link('%link', '<i class="icon-chevron-right"></i>'); ?>				
				</nav>
			</div>

		</section>
		
				
				
		<div class="container-iphone">	
			
			<div class="section-wrapper">		
				<section class="tours-info">
					<div class="contact-details">
					
					<div class="text-block">
						<ul class="info-icons">
							<li>
								<i class="icon-map-marker"></i>
								<span><?php echo get_post_meta($post->ID, 'location', true); ?></span>
							</li>
							
							<li>
								<i class="icon-time"></i>
								<span><?php echo get_post_meta($post->ID, 'days', true); ?></span>
							</li>
							
							<li>
								<i class="icon-tag"></i>
								<span><?php echo get_post_meta($post->ID, 'price', true); ?></span>
							</li>
						</ul>
						
						<!--<i class="icon-map-marker"></i>
						<span>Katalonien, Spanien</span>
					</div>
				
					<div class="text-block">
						<ul>
							<li>
								<i class="icon-time"></i>
								<span>5 tage</span>
							</li>
						
							<li>
								<i class="icon-tag"></i>
								<span>1355 €</span>
							</li>
						</ul>
					</div> -->
					</div>
				<hr>
			</div>
		</section> 
		<div class="section-wrapper">
			<section class="tours-text clearfix">
				<div class="inner">
				<?php 
					
					if(have_posts()) : while(have_posts()) : the_post();
					the_content();
					endwhile;
					endif;
				?>
				
				<?php get_sidebar('tours'); ?>
										
					</div>
			</section>
		</div>
		
					
			<a href="#" class="more-like-this"><i class="icon-refresh icon-spin"></i>See more like this</a>

			<ul class="sections-grid clearfix">	
		
				
				<li class="wrap-contact">
					<a href="<?php bloginfo('url'); ?>/kontakt">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="<?php bloginfo('url'); ?>/buchung" class="open-register">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		
		</div>
		
		</div>
		
		<section class="more-tours" id="desktop-overviews">
									
							<div class="section-wrapper">
							<hgroup class="section-title-wrapper">
								<h2>Weitere Touren</h2>
							</hgroup>
								<div class="footer-inner">
									<ul class="grid-overviews clearfix">
										<?php
											$tours = new WP_Query('post_type=angebot&showposts=3&orderby=rand');
											/*$tours = new WP_Query('cat=4&order=ASC');*/
											$c = 0;
											while($tours -> have_posts()) : $tours -> the_post(); $c++;
											if($c == 3) {
												$style = 'col-last';
												$c = 0;
											}
					
											else $style = '';
											$post_image = atracktive_theme_fetch_post_image(); 
										?>
				
										<li <?php post_class($style); ?>>
											<a href="<?php the_permalink(); ?>">
												<img src="<?php echo $post_image; ?>" alt="Thumbnail">
						
													<div class="tour-details">
														<hgroup>
															<p><?php echo get_post_meta($post->ID, 'tour-subhead', true); ?></p>
															<h2><?php the_title(); ?></h2>
														</hgroup>
					    	
														<dl>
															<dt class="location"><i class="icon-map-marker"></i></dt>
															<dd class="location"><?php echo get_post_meta($post->ID, 'location', true); ?></dd>
															<dt class="duration"><i class="icon-time"></i></dt>
															<dd class="duration"><?php echo get_post_meta($post->ID, 'days', true); ?></dd>
														</dl>
													</div> <!-- /tour-details -->
												
													<a href="<?php the_permalink(); ?>">
														<div class="excerpt-info">
															<hgroup>
																<p class="subhead"><?php echo get_post_meta($post->ID, 'tour-subhead', true); ?></p>
																<h2><?php the_title(); ?></h2>
															</hgroup>
							
															<dl>
																<dt class="location"><i class="icon-map-marker"></i></dt>
																<dd class="location"><?php echo get_post_meta($post->ID, 'location', true); ?></dd>
																<dt class="duration"><i class="icon-time"></i></dt>
																<dd class="duration"><?php echo get_post_meta($post->ID, 'days', true); ?></dd>
														</dl>
							
														<p class="quick-info"><?php atracktive_theme_custom_excerpt(19); ?></p>
							
													</div> <!-- excerpt-info -->
												</a>
											</a>
										</li>
					
										<?php endwhile; ?>
									</ul>
								</div>
									</div>
								</section>
				
		<section class="cta-block">
			<hgroup class="section-title-wrapper">
				<h2>Bist du startbereit?</h2>
				<p>Um das Anmeldeformular auszufüllen, bitte hier klicken</p>
				
				<div class="cta-button"><a href="<?php bloginfo('url'); ?>/buchung/"><i class="icon-arrow-right cta-icon"></i>Buchung</a></div>
			</hgroup>
		</section>
		
		<?php else : ?>
		
		<section class="heading-contacts clearfix">
			<div class="section-wrapper">
				
			<div class="tour-title">
				<h2><?php echo $post->post_title; ?></h2>
			</div>
			
			<nav class="tours-single-controls blog-single-controls">
					<a class="back-button" original-title="Blog" href="<?php bloginfo('url'); ?>/blog"><i class="icon-th"></i></a> <?php next_post_link('%link', '<i class="icon-chevron-left"></i>'); ?><?php previous_post_link('%link', '<i class="icon-chevron-right"></i>'); ?>				
				</nav>		
		</div>
		</section>
		
		
		
				
		<div class="container-iphone">		
		
			<div class="section-wrapper">
			
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
			
			<section class="tours-info">
			<div class="contact-details">
				<div class="text-block">
					<ul class="info-icons">
						<li>
							<i class="icon-calendar"></i>
							<span><?php the_date('j F, Y'); ?></span>
						</li>
							
						<li>
							<i class="icon-pencil"></i>
							<span><?php the_author(); ?></span>
						</li>
							
						<!--<li>
							<i class="icon-comment"></i>
							<span>2 Kommentare</span>
						</li> -->
					</ul>

				</div>
				<hr>
				
			</div>
		</section> 
			<section class="blog-wrap clearfix">
				
				
				<div class="inner">
					
					<div class="col-three-fourth">
					
					<?php 
					
					
					the_content();
					endwhile;
					endif;
					
					?>

						
					<section class="post-gallery clearfix">
						
						<hr>
							
						<!--<h2>Weitere bilder</h2> -->
							
							<?php 
							
							for($i=0;$i<=30;$i++) {
								
								$gallery_thumb = get_post_meta($post->ID, 'gallery-thumb-'.$i, true);
								$gallery_big_image = get_post_meta($post->ID, 'gallery-big-image-'.$i, true);
								
								if (($gallery_thumb) && ($gallery_big_image)) {
									echo "<div class='col-half-inners'><a href='$gallery_big_image' class='lb-trigger'><figure><img src='$gallery_thumb' alt=''></figure></a></div>";
								}
							} 
							
							?>	
							
							<!--<div class="col-half-inners">
								<figure>
									<img src="images/events/siersburg/siersburg_0162_thumb.jpg" alt="">
								</figure>
							</div> 
							
							<div class="col-half-inners">
							
								<a href="http://www.atracktive.com/wp-content/uploads/2012/06/siersburg_0164.jpg" class="lb-trigger">
									<figure>
										<img src="images/events/siersburg/siersburg_0164_thumb.jpg" alt="">
									</figure>
								</a>
								
							</div>
							
							<div class="col-half-inners">
								<figure>
									<img src="images/events/siersburg/siersburg_0165_thumb.jpg" alt="">
								</figure>
							</div>
							
							<div class="col-half-inners">
								<figure>
									<img src="images/events/siersburg/siersburg_0169_thumb.jpg" alt="">
								</figure>
							</div>
							
							<div class="col-half-inners">
								<figure>
									<img src="images/events/siersburg/siersburg_0181_thumb.jpg" alt="">
								</figure>
							</div>
							
							<div class="col-half-inners">
								<figure>
									<img src="images/events/siersburg/siersburg_0198_thumb.jpg" alt="">
								</figure>
							</div> -->
						</section>
					</div>
					
					<div class="one-fourth col-last">
					
					<sidebar class="blog-sidebar">
						
						<h3>Aktuelle artikel</h3>
							
				<section class="latest-posts">
				
				<ul class="grid-overviews clearfix">
				
				<?php 
				
					$recent = new WP_Query('cat=2');
					while ($recent -> have_posts()) : $recent -> the_post();
					
				?>
					<li>
						<a href="<?php the_permalink(); ?>">
							
							<div class="tour-details">
					    		<hgroup>
									<h2><?php the_title(); ?></h2>
									
								</hgroup>
					    	
								<dl>
									<dt class="location"><i class="icon-calendar"></i></dt>
									<dd class="location"><?php the_time(); ?></dd>
									
								</dl>
							</div> <!-- /tour-details -->
						</a>
					</li>
					
					<?php endwhile; ?>		
				</ul>
				
			</section>
					

			</sidebar>
			
				</div>
				
				</div> <!-- /inner -->	
								
			</section>			
			</section>
			
		</div>
		
		
		<ul class="sections-grid clearfix">	
		
				
				<li class="wrap-contact">
					<a href="<?php bloginfo('url'); ?>/kontakt">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="<?php bloginfo('url'); ?>/buchung">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		</div>
		
		
		
	
	<?php endif; ?>


<?php get_footer(); ?>