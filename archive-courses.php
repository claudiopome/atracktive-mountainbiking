<?php
/*
Template Name: Page - Fahrtechnik
*/
?>
<?php get_header(); ?>	
		<section class="heading-contacts clearfix">
			<div class="section-wrapper">
				<div class="tour-title">
				<?php 
					$term = $wp_query->queried_object;?>
					<h2><?php echo $term->name; ?></h2>
				</div>
			
				<nav class="tours-single-controls blog-single-controls">
									 	
				 	<a href="#" class="controls-toggle controls-tours" data-section="categories" original-title="Kategorien"><i class="icon-remove"></i></a>
				</nav>
			</div>

		</section>
		
		<section class="categories-container clearfix" id="categories">
			<div class="section-wrapper">
				<ul>
					<li><span class="select-cat">Auswählen:</span></li>
					<li><a href="<?php bloginfo('url');?>/touren/" data-filter="*" class="selected">Alles</a></li>
					<?php $terms = get_terms("angebot_cat", "order=DESC&hide_empty=0");
								$count = count($terms);
								if ( $count > 0 ){
								
									foreach ( $terms as $term ) {
										echo '<li><a href="' . get_term_link($term->slug, 'angebot_cat').'">' . $term->name . '</a></li>';        
									}
							     } 
						   ?>
				</ul>
			</div>
		</section>
		
				
				
		<div class="container-iphone">	
			
		<div class="section-wrapper">
		
			<section class="more-tours">
				
				<ul class="grid-overviews clearfix">
				<?php
					/*$tours = new WP_Query('cat=4&order=ASC');*/
					$c = 0;
					while(have_posts()) : the_post(); $c++;
					if($c == 3) {
						$style = 'col-last';
						$c = 0;
					}
					
					else $style = '';
						$post_image = atracktive_theme_fetch_post_image(); 
			    ?>
					<li <?php post_class($style); ?>>
						<a href="<?php the_permalink(); ?>">
						<img src="<?php echo $post_image; ?>" alt="Thumbnail">
						
							<div class="tour-details">
					    		<hgroup>
					    			<p><?php echo get_post_meta($post->ID, 'tour-subhead', true); ?></p>
									<h2><?php the_title(); ?></h2>
								</hgroup>
					    	
								<dl>
									<dt class="location"><i class="icon-map-marker"></i></dt>
									<dd class="location"><?php echo get_post_meta($post->ID, 'location', true); ?></dd>
									<dt class="duration"><i class="icon-time"></i></dt>
									<dd class="duration"><?php echo get_post_meta($post->ID, 'days', true); ?></dd>
								</dl>
							</div> <!-- /tour-details -->
						
							<a href="<?php the_permalink(); ?>">
								<div class="excerpt-info">
									<hgroup>
					    				<p class="subhead"><?php echo get_post_meta($post->ID, 'tour-subhead', true); ?></p>
					    				<h2><?php the_title(); ?></h2>
					    			</hgroup>
							
					    			<dl>
						    			<dt class="location"><i class="icon-map-marker"></i></dt>
						    			<dd class="location"><?php echo get_post_meta($post->ID, 'location', true); ?></dd>
						    			<dt class="duration"><i class="icon-time"></i></dt>
						    			<dd class="duration"><?php echo get_post_meta($post->ID, 'days', true); ?></dd>
						    		</dl>
							
						    		<p class="quick-info"><?php atracktive_theme_custom_excerpt(19); ?></p>
							
						    		<!--<a href="#" class="excerpt-info-btn"><i class="icon-search"></i>Mehr</a>-->
						    	</div> <!-- excerpt-info -->
						   </a>
						</a>
					</li>
					
				<?php endwhile; ?>
					
				</ul>

		</div>
		
		<ul class="sections-grid clearfix">	
		
				<li class="wrap-contact">
					<a href="kontakt.html">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="buchung.html" class="open-register">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		
		</div>
		
		</div>
		
				
		<section class="cta-block">
			
			<hgroup class="section-title-wrapper">
				<h2>Für weitere infos</h2>
				<p>Wenn du noch Fragen hast, beantworten wir sie gerne!</p>
				
				<div class="cta-button"><a href="kontakt.html">Schreibe üns hier</a></div>
			</hgroup>

		</section>
		

<?php get_footer(); ?>