<?php
/*
Template Name: Page - About us
*/
?>

	<?php get_header(); ?>
		
		<section class="heading-contacts">
			<div class="section-wrapper">
				<h2><?php echo $post->post_title; ?></h2>
			</div>
		</section>
		
		<div class="container-iphone">		
		
			<section class="about-text clearfix">
				
				
				
				
						
				<div class="inner">
				
				<section class="about-intro clearfix">
				<div class="section-wrapper-xs">
					<div class="footer-inner">
					
					<?php 
					
					if(have_posts()) : while(have_posts()) : the_post();
					the_content();
					endwhile;
					endif;
				?>
				
				
					<div class="text-block" id="axel-thumb">
						
						 <!-- <img src="images/axel-thumb.png" width="161" height="161" alt=""> -->
						 
						<p>&mdash; Axel Molinero, Inhaber von Atracktive</p>
					</div> <!-- /text-block -->
				</div>
				</div>
				</section>
					<section class="team-section clearfix">
					
						<div class="section-wrapper">
						
						<div class="footer-inner">
						
						<hgroup class="section-title-wrapper clearfix">
								<h2>Das Team</h2>
						</hgroup>
						
						
						<section class="team-wrapper">
							<ul class="team-list clearfix">
							
								<li class="post-166 team type-team status-publish hentry">
									<a href="#" class="btn-inactive" data-section="axel-molinero">Axel Molinero<i class="icon-plus-sign"></i></a>
									
									<div id="axel-molinero" class="clearfix closed">
										
										
										<header class="clearfix">
											<h4>Axel Molinero</h4>
										</header>
										
										<p><p class="info-body">Die Liebe zur Natur und die Leidenschaft für den Ausdauersport sind zwei Eigenschaften die ihn sehr gut charakterisieren. Neben dem Laufen, dass er sehr gerne, auch wettkampfmäßig, betreibt, gilt dem Mountainbiken seine große Leidenschaft.</p><p class="info-body">Nach dem Studium der Sportwissenschaften und der Geographie an der Universität in Göttingen, ist er professionell in die Mountainbike-Szene eingestiegen.</p><p class="info-body">Axel freut sich, einen Beruf ausüben zu können, bei dem die Kommunikation mit Menschen im Mittelpunkt steht und bei dem er sein Fachwissen über Training und Methodik im Freizeit- und Leistungssport vermitteln kann.</p><p class="info-body">Er möchte seine Faszination für das Mountainbiken auf alle übertragen, die sich für diese Sportart interessieren oder bereits selbst begeisterte Mountainbiker sind.</p></p>
										
									</div> <!-- /axel -->
								</li>
								
								
								<li>
									<a href="#" class="btn-inactive" data-section="massimo-casorzo">Massimo Casorzo<i class="icon-plus-sign"></i></a>
									
									<div id="massimo-casorzo" class="clearfix closed">
										
										
										<header class="clearfix">
											<h4>Massimo Casorzo</h4>
										</header>
										
										<p><p class="info-body">Die Berge, die Natur und der Sport sind seine großen Leidenschaften. Seit seiner Jugend liebt es Massimo, Sport im Freien zu treiben.</p><p class="info-body">Seine Leidenschaft gilt vor allem dem Skilanglauf, den er wettkampfmäßig betreibt. Im Sommer tauscht Massimo die Ski gegen sein Bike ein.</p><p class="info-body">Das Mountainbiken hält ihn während der langen Sommermonate fit und ermöglicht es ihm, seinen Freiheitsdrang zu stillen. Seit Kindertagen fährt er auf den fantastischen Trails der Westalpentäler, wo er sich bestens auskennt. Bei jeder Fahrt gibt es etwas Neues, Überraschendes und Aufregendes zu entdecken.</p><p class="info-body">Aus diesem Grund hat Massimo beschlossen, Biker zu guiden und mit seinen Entdeckungstouren zu begeistern. Die Zusammenarbeit mit Atracktive erlaubt es ihm, dies in professionellem Rahmen zu tun.</p></p>
										
									</div> <!-- /massimo -->
								</li>
								</ul>
								
								<ul class="team-list clearfix">
								
								<li>
									<a href="#" class="btn-inactive" data-section="florian-mair">Florian Mair<i class="icon-plus-sign"></i></a>
									
									<div id="florian-mair" class="clearfix closed">
										
										
										<header class="clearfix">
											<h4>Florian Mair</h4>
										</header>
										
										<p><p class="info-body">Im Herzen der Alpen in Innsbruck aufgewachsen, ist Florian seit seiner Kindheit den Bergen verbunden, und seit seinem 14. Lebensjahr auf dem Mountainbike unterwegs.</p><p class="info-body">Seine große Leidenschaft sind Mehrtagestouren von Tal zu Tal und alpine Mountainbiketouren, die oft erst am Gipfel enden. Seit 2013 ist er ausgebildeter Bikeguide (DIMB) und arbeitet mit Atracktive zusammen.</p><p class="info-body">Wenn er nicht mit dem Bike in den Alpen unterwegs ist, dann mit dem Fotoapparat auf der Suche nach der Schönheit der Berge. Seine Erlebnisse kann man auf seinem Blog  <a href="http://www.alpine-biking.com" target="_blank">www.alpine-biking.com</a> verfolgen.</p></p>
										
									</div> <!-- /florian -->
								</li>
								
								
								
								<li>
									<a href="#" class="btn-inactive" data-section="norbert-martini">Norbert Martini<i class="icon-plus-sign"></i></a>
									
									<div id="norbert-martini" class="clearfix closed">
										
										
										<header class="clearfix">
											<h4>Norbert Martini</h4>
										</header>
										
										<p><p class="info-body">Norbert ist seit über zwanzig Jahren begeisterter Mountainbiker. Am liebsten ist er auf den Trails seiner saarländischen Heimat oder in den Alpen unterwegs.</p><p class="info-body">Mountainbiken ist für Norbert eine Lebenseinstellung. Bei fast jedem Wetter nutzt er sein Bike, um zur Arbeit zu kommen. Norbert ist Lehrer an einer Realschule. Dort leitet er auch seit vielen Jahren eine Mountainbike-AG.</p><p class="info-body">Nicht nur Norbert, sondern auch ein Großteil seiner Familie ist vom Mountainbike-Virus befallen. Seine Frau Elisabeth und die beiden Söhne Benjamin und Jonas nehmen regelmäßig an verschiedenen MTB-Rennen (Cups, Marathons) teil. Seine Erfahrungen gibt Norbert auf seiner Website <a href="http://www.nmbiking.de" target="_blank">www.nmbiking.de</a> in Form von Tagebucheinträgen, Tourenberichten und GPS-Daten an interessierte Mountainbiker weiter.</p></p>
										
									</div> <!-- /norbert -->
								</li>
								
		
							</ul>
						</section> <!-- /team-wrapper -->
						</div>
						</div>
					</section> <!-- /team-section -->
					
					<section class="partners-section clearfix">
						<div class="section-wrapper">
							<hgroup class="section-title-wrapper clearfix">
								<h2>Partners</h2>
							</hgroup>
							
							<ul class="partners-list">
								<li><a href="http://www.alpibikeresort.com" target="_blank"><img src="http://www.atracktive.com/wp-content/uploads/2014/04/alpi-bike.jpg" alt="Alpi Bike" /></a></li>
								<li><a href="http://www.bikeaid.de" target="_blank"><img src="http://www.atracktive.com/wp-content/uploads/2014/04/bike-aid.jpg" alt="Bike AID" /></a></li>
								
								<li><a href="http://www.chaletdellaguida.it" target="_blank"><img src="http://www.atracktive.com/wp-content/uploads/2014/04/chalet-guida.jpg" alt="Chalet della Guida" /></a></li>
								
								<li><a href="http://www.worldofmtb.de" target="_blank"><img src="http://www.atracktive.com/wp-content/uploads/2014/04/mtb-magazin.jpg" alt="MTB Magazin" /></a></li>
								<li><a href="http://www.sagnarotonda.com" target="_blank"><img src="http://www.atracktive.com/wp-content/uploads/2014/04/sagna-rotonda.jpg" alt="Sagna Rotonda" /></a></li>
								
								<li><a href="http://www.turismotorino.org" target="_blank"><img src="http://www.atracktive.com/wp-content/uploads/2014/04/turismo-torino.jpg" alt="Turismo Torino e Provincia" /></a></li>
							</ul>
						</div>
					</section>
			</div>
				
		</section>

			
			<!--<section class="contact-info" id="info-mobile">
			
			<div class="contact-details">
				<div class="text-block">
					<i class="icon-home"></i>
					<span>Calsowstr. 13 - 37085 Göttingen</span>
				</div>
				
				<div class="text-block">
					<ul>
						<li>
							<i class="icon-phone"></i>
								<a href="tel:+49 172 539 09 36"><span>+49 172 539 09 36</span></a>
						</li>
						<li>
							<i class="icon-envelope-alt"></i>
							
							<a href="mailto:info@atracktive.com"><span>info@atracktive.com</span></a>
						</li>
					</ul>
				</div>
			
		</section> -->

			
			<ul class="sections-grid clearfix">	
		
				<li class="wrap-contact">
					<a href="<?php bloginfo('url'); ?>/kontakt/">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="<?php bloginfo('url'); ?>/buchung/" class="open-register">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		</div> <!-- /container-iphone -->
		
		<section class="cta-block">
			
			<hgroup class="section-title-wrapper">
				<h2>Für weitere infos</h2>
				<p>Wenn du noch Fragen hast, beantworten wir sie gerne!</p>
				
				<div class="cta-button"><a href="<?php bloginfo('url'); ?>/kontakt/"><i class="icon-envelope-alt cta-icon"></i>Schreibe uns hier</a></div>
			</hgroup>

		</section>

		<?php get_footer(); ?>