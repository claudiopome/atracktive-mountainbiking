	<div class="col-half col-last">
					
					<div class="carousel-wrap">
							<div class="carousel-list-wrap">
								<ul>
									<?php 
										for ($i=0; $i<=20; $i++) {
											
											$gallery_image = get_post_meta($post->ID, 'gallery-image-'.$i, true);
											if($gallery_image) {
												echo '<li><img src="' . $gallery_image . '"></li>'; 
											}
										}
									 ?>
								</ul>
							</div>
							<nav>
								<a href="#" class="prev"><i class="icon-circle-arrow-left"></i></a>
								<a href="#" class="next"><i class="icon-circle-arrow-right"></i></a>
							</nav>
						</div>

						<section class="facts clearfix">
								<div class="facts-info">
								
								<header class="clearfix">
									<div class="wrap-figure">
										<i class="icon-info-sign"></i>
									</div>
									<div class="wrap-details">
									<span>Tour facts</span>
									</div>
								</header>
								
									<div class="facts-wrap clearfix">
										
										<ul>
											<li>Anzahl Touren:<span><?php echo get_post_meta($post->ID, 'anzhal-touren', true); ?></span></li>
											<li><a href="<?php bloginfo('url'); ?>/faq/#level-1">Kondition:</a><span><?php echo get_post_meta($post->ID, 'kondition', true); ?></span></li>
											<li><a href="<?php bloginfo('url'); ?>/faq/#level-1">Technik:</a><span><?php echo get_post_meta($post->ID, 'technik', true); ?></span></li>
											<li>Kilometer:<span><?php echo get_post_meta($post->ID, 'kilometer', true); ?></span></li>
											<li>Höhenmeter:<span><?php echo get_post_meta($post->ID, 'hoenmeter', true); ?></span></li>
										</ul>
									</div>
									
								</div>
								
								<div class="dates">
									<header class="clearfix">
									<div class="wrap-figure">
										<i class="icon-calendar"></i>
									</div>
									<div class="wrap-details">
									<span>Termine</span>
									</div>
								</header>
								
								<div class="facts-wrap clearfix">
									<ul>
									<?php 
										for ($i=0; $i<=5; $i++) {
											
											$date = get_post_meta($post->ID, 'termin-'.$i, true);
											if($date) {
												echo '<li>' . $date . '</li>'; 
											}
										}
									 ?>
									</ul>
								</div>
								</div>
								
								<div class="included">
									<header class="clearfix">
									<div class="wrap-figure">
										<i class="icon-ok"></i>
									</div>
									<div class="wrap-details">
									<span>Leistungen</span>
									</div>
								</header>
								
								<div class="facts-wrap clearfix">
									<ul>
									<?php 
										for ($i=0; $i<=10; $i++) {
											
											$included = get_post_meta($post->ID, 'leistungen-'.$i, true);
											if($included) {
												echo '<li>' . $included . '</li>'; 
											}
										}
									 ?>
									</ul>
								</div>
								
							</div> <!-- /included -->
								
								<div class="dates">
									<header class="clearfix">
									<div class="wrap-figure">
										<i class="icon-remove"></i>
									</div>
									<div class="wrap-details">
									<span>Nicht Inklusive</span>
									</div>
								</header>
								
								<div class="facts-wrap clearfix">
									<ul>
									<?php 
										for ($i=0; $i<=10; $i++) {
											
											$not_included = get_post_meta($post->ID, 'nicht-inklusive-'.$i, true);
											if($not_included) {
												echo '<li>' . $not_included . '</li>'; 
											}
										}
									 ?>
									</ul>

								</div>
								
								<div class="dates">
									<header class="clearfix">
									<div class="wrap-figure">
										<i class="icon-plus"></i>
									</div>
									<div class="wrap-details">
									<span>Optionals</span>
									</div>
								</header>
								
								<div class="facts-wrap clearfix">
									<ul>
									<?php 
										for ($i=0; $i<=5; $i++) {
											
											$optional = get_post_meta($post->ID, 'optional-'.$i, true);
											if($optional) {
												echo '<li>' . $optional . '</li>'; 
											}
										}
									 ?>
									</ul>

								</div>

							</div> <!-- not-included -->
							</section>


					</div>
