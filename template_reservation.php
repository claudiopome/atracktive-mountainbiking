<?php
/*
Template Name: Page - Buchung
*/
?>

<?php get_header(); ?>
		
		<section class="heading-contacts">
			<div class="section-wrapper">
				<h2><?php echo $post->post_title; ?></h2>
			</div>
		</section>
		
	
		
		<div class="container-iphone">		
		
		
			<section class="form clearfix" id="form-register">
				<div class="section-wrapper">
				
				<?php 
				if (have_posts()) : while(have_posts()) : the_post();
				the_content();
				endwhile;
				endif;
			?>
				
				<!--<form action="#" method="post" class="contact-form">
					<header class="clearfix">
						<div class="wrap-figure"><i class="icon-arrow-right"></i></div>
						<div class="wrap-details">
							<h2>Buchung</h2>
						</div>
					</header>
					<div class="wrap">
						<div class="col-half">
						
							<label for="name">Name</label>
							<input type="text" name="name" id="name" placeholder="Name" class="required">
						
							<label for="surname">Vorname</label>
							<input type="text" name="surname" id="surname" placeholder="Vorname" class="required">
											
							<label for="email">E-mail Addresse</label>				
							<input type="email" name="email" id="email" placeholder="E-mail Adresse" class="required">
										
							<label for="phone-number">Telefon</label>
							<input type="text" name="phone-number" id="phone-number" placeholder="Telefon" class="required">
							
							<label for="street">Strasse</label>
							<input type="text" name="street" id="street" placeholder="Strasse" class="required">
							
							<label for="plz">PLZ</label>
							<input type="text" name="plz" id="plz" placeholder="PLZ" class="required">
						</div>
						
						<div class="col-half col-last">	
							
							<label for="location">Ort</label>
							<input type="text" name="location" id="location" placeholder="Ort">
							
							
							<label>Event</label>
							<div id="dd" class="wrapper-dropdown"><span>Event</span>
									<ul class="dropdown">
										<li><a href="#" data-filter="*" class="selected">All</a></li>
										<li><a href="#" data-filter=".tours">Touren</a></li>
										<li><a href="#" data-filter=".courses">Fahrtechnik</a></li>
									</ul>
							</div>
							
							<label>Anzahl Personen</label>
							<div id="dd" class="wrapper-dropdown"><span>Anzahl Personen</span>
									<ul class="dropdown">
										<li><a href="#" data-filter="*" class="selected">All</a></li>
										<li><a href="#" data-filter=".tours">Touren</a></li>
										<li><a href="#" data-filter=".courses">Fahrtechnik</a></li>
									</ul>
							</div>
							
							<label for="subject">Betreff</label>
							<input type="text" name="subject" id="subject" placeholder="Betreff" class="required">
							
							<label for="message">Sostinges</label>
							<textarea name="message" id="message" placeholder="Sostinges"></textarea>
											
					<p><button type="submit" id="abschicken">Abschicken</button></p>
					<p><button type="submit" id="abbrechen">Abbrechen</button></p>

						</div>
					</div>
				</form> -->
				</div>
			</section>


			<ul class="sections-grid clearfix">	
		
				<li class="wrap-contact">
					<a href="<?php bloginfo('url'); ?>/kontakt/">
						<hgroup class="section-head">
							<h2>Kontakt</h2>
						</hgroup>
					</a>
				</li>
		
				<li class="wrap-newsletter">
					<a href="<?php bloginfo('url'); ?>/buchung/">
						<hgroup class="section-head">
							<h2>Buchung</h2>
						</hgroup>
					</a>
				</li>
		</ul>
		</div>
		
		<section class="cta-block">
			
			<hgroup class="section-title-wrapper">
				
				<div class="twitter-icon">
					<i class="icon-twitter"></i>	
				</div>
				
				<div class="section-wrapper">
					<p>Eine Reise durch die verlassenen T&auml;ler des Piemont <a href="http://vimeo.com/69452733" target="_blank">http://vimeo.com/69452733</a> Danke an Ivan f&uuml;r das tolle Video!</p>
				</div>
				
				<p class="follow">Folge Atracktive auf <a href="https://twitter.com/Atracktive_mtb" target="_blank">Twitter</a></p>
			</hgroup>
		</section>
		
<?php get_footer(); ?>